package com.bit.bitcare.mcflp2nd.sign.utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class PreferenceUtil {

    //프리퍼런스 KEY값
    public final String HANBAE_PREFERENCE = "MCFLP_2nd_preference";
    public final String HANBAE_PREFERENCE_NOT_ENCRYPT= "MCFLP_2nd_preference_not_encrypt";
    private PreferenceUtil kpointPreferencemodule = null;
    private Context mContext;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;

    public PreferenceUtil(Context context) {
        mContext = context;
        if (prefs == null) {
//            prefs = mContext.getSharedPreferences(HANBAE_PREFERENCE, Context.MODE_PRIVATE);
            prefs = new ObscuredSharedPreferences(mContext,mContext.getSharedPreferences(HANBAE_PREFERENCE, MODE_PRIVATE));
            editor = prefs.edit();
        }
    }

    public PreferenceUtil(Context context, String notEncrypt) {
        mContext = context;
        if (prefs == null) {
            prefs = mContext.getSharedPreferences(HANBAE_PREFERENCE_NOT_ENCRYPT, Context.MODE_PRIVATE);
            editor = prefs.edit();
        }
    }

    public void putFloatExtra(String key, float value){
        editor.putFloat(key, value);
        editor.apply();
    }

    public void putIntExtra(String key, int value) {
        editor.putInt(key, value);
        editor.apply();
    }

    public void putStringExtra(String key, String value) {
        editor.putString(key, value);
        editor.apply();
    }

    public void putLongExtra(String key, long value) {
        editor.putLong(key, value);
        editor.apply();
    }

    public void putBooleanExtra(String key, boolean value) {
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void clear() {
        editor.clear();
        editor.apply();
    }


    public int getIntExtra(String key) {
        return prefs.getInt(key, 0);
    }

    public String getStringExtra(String key) { return prefs.getString(key, ""); }

    public float getFloatExtra(String key) { return prefs.getFloat(key, 0); }

    public long getLongExtra(String key) {
        return prefs.getLong(key, 0);
    }

    public boolean getBooleanExtra(String key) {
        return prefs.getBoolean(key, false);
    }

    public void removePreference(String key) {
        editor.remove(key).commit();
    }

    public boolean containCheck(String key) {
        return prefs.contains(key);
    }


    public Map<String, ?> getAll(){ return prefs.getAll(); }
}
