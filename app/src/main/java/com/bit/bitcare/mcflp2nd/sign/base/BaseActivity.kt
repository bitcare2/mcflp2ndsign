package com.bit.bitcare.mcflp2nd.sign.base

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.StringRequestListener

import java.util.ArrayList
import org.json.JSONArray
import com.androidnetworking.interfaces.JSONArrayRequestListener
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.bit.bitcare.mcflp2nd.sign.utils.StringConverter
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.LongSerializationPolicy
import org.json.JSONObject


open class BaseActivity : AppCompatActivity() {

//    //Typekit 적용
//    override fun attachBaseContext(newBase: Context?) {
//        super.attachBaseContext(TypekitContextWrapper.wrap(newBase))
//    }

    //    var gson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create()
//    var gb: GsonBuilder  = GsonBuilder()
//    gb.registerTypeAdapter(String.class, new StringConverter());
//    Gson gson = gb.create();
//    var gson = GsonBuilder().registerTypeAdapter(String::class.java, StringConverter()).create()

    var gson = GsonBuilder().disableHtmlEscaping().create()

//    var gson = GsonBuilder().disableHtmlEscaping().create()

//    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
//        super.onCreate(savedInstanceState, persistentState)
//        var gsonBuilder = GsonBuilder()
//        gsonBuilder.setLongSerializationPolicy(LongSerializationPolicy.STRING)
//        gson = gsonBuilder.serializeNulls().create()
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        } catch (e: Exception) {

        }
    }

    override fun onResume() {
        super.onResume()
        window.addFlags(WindowManager.LayoutParams.FLAG_SECURE)

        val serverIp = PreferenceUtil(this@BaseActivity).getStringExtra("serverIP")
        Define.serverIP = serverIp

//        Define.serverIP = "http://nibp-mobile.g2e.co.kr"
//        Define.ozEndPoint = "http://nibp-report.g2e.co.kr/oz70/server"
    }

    override fun setRequestedOrientation(requestedOrientation: Int) {
        /*
         if (Build.VERSION.SDK_INT == Build.VERSION_CODES.O) {
             // no-op
         }else{
             super.setRequestedOrientation(requestedOrientation)
         }
         */
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            super.setRequestedOrientation(requestedOrientation)
        }
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(0,0)
    }

    open fun updateToken(){
        AndroidNetworking.get("http://59.10.164.97:1983/MCFLPForMobileAPI/cert/token?userId=12")
                .addPathParameter("pageNumber", "0")
                .addQueryParameter("limit", "3")
                .addHeaders("token", "1234")
                .setTag("test")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
//                        Dlog.e("받아옴?")
//                        Dlog.e(response.toString())
                        var loginVo = gson.fromJson(response.toString(), ResponseVo::class.java)
                        Dlog.e(loginVo.toString())
                        PreferenceUtil(this@BaseActivity).putStringExtra("token",loginVo.result.token)
                    }

                    override fun onError(error: ANError) {
                        // handle error
                    }
                })
    }

    private val stringRequestListener = object: StringRequestListener {
        override fun onResponse(response:String) {
            Log.e("send file onResponse",response.toString())
            Toast.makeText(this@BaseActivity, "토큰 받아옴", Toast.LENGTH_LONG)
            PreferenceUtil(this@BaseActivity).putStringExtra("token",response.toString())
        }
        override fun onError(error: ANError) {
            val errorCode = arrayListOf(error.errorCode)
            if (errorCode[0] !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Log.d("send file onError", "onError errorCode : " + error.errorCode)
                Log.d("send file onError", "onError errorBody : " + error.errorBody)
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
                // get parsed error object (If ApiError is your class)
//                    val apiError = error.getErrorAsObject(ApiError::class.java)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Log.d("send file onError", "onError errorDetail : " + error.errorDetail)
            }
        }
    }


    //권한 체크
    fun checkPermissions(permissions: Array<String>, permissionResultCode: Int): Boolean {
        var result: Int
        val permissionList = ArrayList<String>()
        for (pm in permissions) {
            result = ContextCompat.checkSelfPermission(this, pm)
            if (result != PackageManager.PERMISSION_GRANTED) { //사용자가 해당 권한을 가지고 있지 않을 경우 리스트에 해당 권한명 추가
                permissionList.add(pm)
                Dlog.e("permissionList : $pm")
            }
        }
        if (!permissionList.isEmpty()) { //권한이 추가되었으면 해당 리스트가 empty가 아니므로 request 즉 권한을 요청합니다.
            Dlog.e("permissionList : ${permissionList}")
            requestPermissions(this, permissionList.toTypedArray(), permissionResultCode)
            return false
        }
        return true
    }

    //권한 요청
    fun requestPermissions(requireActivity: Activity, toTypedArray: Array<String>, permissionResultCode: Int) {
        when (permissionResultCode) {
            Define.REQUEST_RECORD_VOICE_PERMISSION -> {
                for (permission in Define.needVoiceRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVoiceRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_RECORD_VIDEO_PERMISSION -> {
                for (permission in Define.needVideoRecordPermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needVideoRecordPermissions, permissionResultCode)
                        break
                    }
                }
            }

            Define.REQUEST_FILE_PERMISSION -> {
                for (permission in Define.needFilePermissions) {
                    if (ActivityCompat.checkSelfPermission(baseContext, permission) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(requireActivity, Define.needFilePermissions, permissionResultCode)
                        break
                    }
                }
            }

        }
    }



}
