package com.bit.bitcare.mcflp2nd.data.model

data class ResultVo(
    var token: String,
    var result: String
)