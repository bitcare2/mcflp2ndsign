package com.bit.bitcare.mcflp2nd.sign.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.PersistableBundle
import android.provider.Settings
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo

import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.*

import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util


import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.PendingDynamicLinkData
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_certification_code.*
import kotlinx.android.synthetic.main.activity_splash.*
import org.json.JSONObject
import java.lang.Thread.sleep
import java.util.*
import kotlin.coroutines.experimental.EmptyCoroutineContext
import kotlin.math.sign

class SplashDialogActivity : BaseActivity() {
    private lateinit var code:String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_dialog)

        code = intent?.extras?.getString("code")!!

    }

    override fun onResume() {
        super.onResume()

        MaterialDialog(this@SplashDialogActivity)
                .title(R.string.ask_logout_title)
                .message(R.string.ask_logout_message)
                .positiveButton(R.string.yes) {
                    Dlog.e("종료 - 예")
                    PreferenceUtil(this@SplashDialogActivity).clear()

                    val intent = Intent(this@SplashDialogActivity, CertificationCodeActivity::class.java)
                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                    intent.putExtra("code", code)
                    startActivity(intent)
                    overridePendingTransition(0, 0)
                    finish()
                }
                .negativeButton(R.string.no) {
                    Dlog.e("종료 - 아니오")
                    finish()
                }
                .show{
                    setCancelable(false)
                    setCanceledOnTouchOutside(false)
                }
    }

    
}

