package com.bit.bitcare.mcflp2nd.sign.ui.signature;

public class DateCheckException extends RuntimeException {

    public DateCheckException(String value){
        super(value);
    }

}
