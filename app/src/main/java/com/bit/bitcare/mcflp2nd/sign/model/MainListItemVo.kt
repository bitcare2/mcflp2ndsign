package com.bit.bitcare.mcflp2nd.sign.model


data class MainListItemVo(
        val name: String?,
        val gender: String?,
        val date: String?,
        val title: String?,
        val type: String?,
        val number: String?,
        val state: String?,
        val signFormSN: String?,
        val formSN: String?
)