package com.bit.bitcare.mcflp2nd.sign.model


data class LstplanDetailInfoVO(
// @ApiModelProperty(value = "연명의료계획서 일련번호", example = "123456")
        var lstplansn: Int?,                    //bigint NOT NULL DEFAULT nextval('seq_t_lstplan'::regclass),
// @ApiModelProperty(value = "연명의료계획서 일련번호", example = "123456")
        var lstplantempsn: Int?,                    //bigint NOT NULL DEFAULT nextval('seq_t_lstplan'::regclass),
// @ApiModelProperty(value = "연명의료계획서 상태코드", example = "01")
        var lstplanstatcode: String?,             //character varying(2) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "의료기관번호", example = "123456")
        var orgmedno: String?,                    //character varying(9) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "등록번호", example = "123456")
        var regno: String?,                       //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "삭제여부", example = "N")
        var delyn: String?,                       //character varying(1) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자명", example = "123456")
        var patiusernm: String?,                  //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자주민등록번호", example = "7412201111111")
        var patiresid: String?,                   //character varying(14) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자주민등록번호(암호화)", example = "")
        var patiresidenc: String?,
// @ApiModelProperty(value = "환자생년월일", example = "1974/12/20")
        var patibday: String?,                    //date NOT NULL,
// @ApiModelProperty(value = "환자성별", example = "M")
        var patigender: String?,                  //character varying(1) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자우편번호", example = "123456")
        var patizipno: String?,                   //character varying(6) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자주소", example = "서울 관악구")
        var patiaddr1: String?,                   //character varying(100) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자주소상세", example = "서울대병원")
        var patiaddr2: String?,                   //character varying(100) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "법정동코드", example = "123456")
        var bjdcode: String?,                     //character varying(10) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자연락처", example = "02-1234-2345")
        var patitn: String?,                      //character varying(14) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자상태코드", example = "02")
        var patistatecode: String?,               //character varying(2) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "담당의사성명", example = "123456")
        var docnm: String?,                       //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "담당의사면허번호", example = "123456")
        var doclicence: String?,                  //character varying(10) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "담당의사소속기관명", example = "123456")
        var docorgmednm: String?,                 //character varying(55) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "호스피스이용계획 YN", example = "Y")
        var hosuseyn: String?,                    //character varying(1) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "본인여부 YN(Y : 본인, N : 법정대리인)", example = "Y")
        var selfyn: String?,                      //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "확인방법코드", example = "01")
        var confrmethcode: String?,               //character varying(2) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자서명이미지파일(DB삭제::사용안함)", example = "123456")
        var patisignimgfilegrpno: String?,        //bigint,
// @ApiModelProperty(value = "환자녹화파일", example = "123456")
        var patividfilegrpno: String?,            //bigint,
// @ApiModelProperty(value = "환자녹취파일", example = "123456")
        var patitypfilegrpno: String?,            //bigint,
// @ApiModelProperty(value = "확인일", example = "123456")
        var confrday: String?,                    //date NOT NULL,
// @ApiModelProperty(value = "확인 사인 이미지", example = "123456")
        var confsignimgfilegrpno: String?,        //bigint,
// @ApiModelProperty(value = "법정대리인성명", example = "123456")
        var repusernm: String?,                   //character varying(50) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "법정대리인사인이미지", example = "123456")
        var repsignimgfilegrpno: String?,         //bigint,
// @ApiModelProperty(value = "환자사망전열람허용코드", example = "01")
        var readacccode: String?,                 //character varying(2) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "환자사망전열람기타의견", example = "123456")
        var readaccoth: String?,                  //text COLLATE pg_catalog."default",
// @ApiModelProperty(value = "등록일", example = "2017/12/08")
        var planday: String?,                     //date NOT NULL,
// @ApiModelProperty(value = "담당의사 사용자 일련번호", example = "123456")
        var docusersn: Int?,                   //bigint NOT NULL,
// @ApiModelProperty(value = "담당의사 사인 이미지", example = "123456")
        var docsignimgfilegrpno: String?,         //bigint,
// @ApiModelProperty(value = "원본스캔파일 파일그룹번호", example = "123456")
        var orignfilegrpno: String?,              //bigint,
// @ApiModelProperty(value = "등록프로그램", example = "123456")
        var enterpgm: String?,                    //character varying(200) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "등록일시", example = "2017/12/08")
        var enterdate: String?,                   //timestamp without time zone NOT NULL,
// @ApiModelProperty(value = "등록자아이디", example = "123456")
        var enterid: String?,                     //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "등록자명", example = "123456")
        var enternm: String?,                     //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "수정프로그램", example = "123456")
        var updatepgm: String?,                   //character varying(200) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "수정일시", example = "2017/12/08")
        var updatedate: String?,                  //timestamp without time zone,
// @ApiModelProperty(value = "수정자아이디", example = "123456")
        var updateid: String?,                    //character varying(50) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "수정자명", example = "123456")
        var updatenm: String?,                    //character varying(50) COLLATE pg_catalog."default",
// @ApiModelProperty(value = "철회사유", example = "123456")
        var rvocrsn: String?,
// @ApiModelProperty(value = "비고", example = "123456")
        var rmk: String?,

// @ApiModelProperty(value = "연명의료계획서 설명사항 첫번째", example="Y")
        var explan1: String?,
// @ApiModelProperty(value = "연명의료계획서 설명사항 두번째", example="Y")
        var explan2: String?,
// @ApiModelProperty(value = "연명의료계획서 설명사항 세번째", example="Y")
        var explan3: String?,
// @ApiModelProperty(value = "연명의료계획서 설명사항 네번째", example="Y")
        var explan4: String?,
// @ApiModelProperty(value = "연명의료계획서 설명사항 다섯번째", example="Y")
        var explan5: String?,
// @ApiModelProperty(value = "연명의료계획서 설명사항 여섯번째", example="Y")
        var explan6: String?,
// @ApiModelProperty(value = "연명의료계획서 결정항목 첫번째", example="Y")
        var item1: String?,
// @ApiModelProperty(value = "연명의료계획서 결정항목 두번째", example="Y")
        var item2: String?,
// @ApiModelProperty(value = "연명의료계획서 결정항목 세번째", example="Y")
        var item3: String?,
// @ApiModelProperty(value = "연명의료계획서 결정항목 네번째", example="Y")
        var item4: String?,
// 법정대리인 확인일
        var repconfrday: String?,

        var mnrver: String?
)
