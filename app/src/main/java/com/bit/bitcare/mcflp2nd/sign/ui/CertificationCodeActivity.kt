package com.bit.bitcare.mcflp2nd.sign.ui

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.content.ContextCompat.startActivity
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.R.id.bt_certification_code
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_certification_code.*
import org.json.JSONObject
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import com.bit.bitcare.mcflp2nd.sign.model.CheckLoginKeyVo
import com.bit.bitcare.mcflp2nd.util.Util


class CertificationCodeActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_certification_code)

        var deepLinkCode: String

        if (intent.hasExtra("code")) {
            deepLinkCode = intent.getStringExtra("code")
            Toast.makeText(this@CertificationCodeActivity, "수신 코드 : " + deepLinkCode, Toast.LENGTH_LONG).show()
            Dlog.e("코드 : " + deepLinkCode)
            goToCertificationCodeActivity(deepLinkCode)
        }
        et_certification_code_input_key.imeOptions = EditorInfo.IME_ACTION_DONE
        et_certification_code_input_key.setOnEditorActionListener(TextView.OnEditorActionListener { textView, i, keyEvent ->
            when(i){
                EditorInfo.IME_ACTION_DONE ->{
                    var code = et_certification_code_input_key.getText().toString().trim()
                    goToCertificationCodeActivity(code)
                }
            }
            return@OnEditorActionListener false
        })
        et_certification_code_input_key.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            }

            override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int,
                                           arg3: Int) {
            }

            override fun afterTextChanged(et: Editable) {
                var s = et.toString()
                if (s != s.toUpperCase()) {
                    s = s.toUpperCase()
                    et_certification_code_input_key.setText(s)
                }
                et_certification_code_input_key.setSelection(et_certification_code_input_key.getText().length)
            }
        });

        bt_certification_code.setOnClickListener {
            var code = et_certification_code_input_key.getText().toString().trim()
            goToCertificationCodeActivity(code)
        }
    }

    private fun goToCertificationCodeActivity(certificationCode:String) {
        val fcmToken = FirebaseInstanceId.getInstance().token

        if (fcmToken != null) {
            Dlog.e("fcmToken = $fcmToken")
        } else {
            Dlog.e("fcmToken is null")
            Toast.makeText(this@CertificationCodeActivity, "토큰을 만드는데 실패했습니다.", Toast.LENGTH_LONG).show()
        }

        val androidID = Settings.Secure.getString(baseContext.contentResolver, Settings.Secure.ANDROID_ID)
        PreferenceUtil(this).putStringExtra("androidID", androidID)
        Dlog.e("androidID = $androidID")

        var loginType = ""
        Dlog.e("code.length =" + certificationCode.length)

        if (certificationCode.length != 0 && certificationCode.length == 7) {
            loginType = Util.checkLoginType(certificationCode)
        }else{
            Toast.makeText(this,"SMS 메세지를 확인하시고\n올바른 인증 번호를 입력해 주세요.",Toast.LENGTH_LONG).show()
            return
        }

        Dlog.e("seok Define.serverIP: " + Define.serverIP)

        AndroidNetworking.post(Define.serverIP+"/sign/checkLoginkey")
                .addBodyParameter("deviceType", "2") // OS타입(1 : 아이폰, 2 : 안드로이드)
                .addBodyParameter("loginType", loginType) //  03: 웹사이트인증 C, 04: 서식서명인증 S, 05: 협진인증 A
                .addBodyParameter("loginKey", certificationCode)
                .addBodyParameter("deviceSID", androidID)
                .addBodyParameter("deviceRegID", fcmToken)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("받아옴??")
                        Dlog.e(response.toString())
                        var checkLoginKeyVo = gson.fromJson(response.toString(), CheckLoginKeyVo::class.java)

                        if("100".equals(checkLoginKeyVo.serviceCode)){
                            Dlog.e(checkLoginKeyVo.toString())
                            Dlog.e(checkLoginKeyVo.result.toString())

                            PreferenceUtil(this@CertificationCodeActivity).putStringExtra("checkLoginKeyResultVo",gson.toJson(checkLoginKeyVo.result))
                            PreferenceUtil(this@CertificationCodeActivity).putStringExtra("signFormSN",gson.toJson(checkLoginKeyVo.result.signFormSN))

                            val intent = Intent(this@CertificationCodeActivity, CertificationInformationActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            startActivity(intent)
                            overridePendingTransition(0,0)
                        }
                        else{
                            Toast.makeText(this@CertificationCodeActivity,"인증코드가 맞지 않습니다.\n다시 한번 시도해 주세요.",Toast.LENGTH_LONG).show()
                            et_certification_code_input_key.setText(certificationCode)
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("에러??")
                        Toast.makeText(this@CertificationCodeActivity,"SMS 메세지를 확인하시고\n올바른 인증 번호를 입력해 주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

    fun viewError(message: String) {
        MaterialDialog(this)
                .title(R.string.fail_login)
                .message(text = message)
                .positiveButton (R.string.done){
                    finish()
                }
                .show()
    }
}