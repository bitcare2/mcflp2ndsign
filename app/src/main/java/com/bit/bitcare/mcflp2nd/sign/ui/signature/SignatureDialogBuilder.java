package com.bit.bitcare.mcflp2nd.sign.ui.signature;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bit.bitcare.mcflp2nd.sign.R;
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog;
import com.bit.bitcare.mcflp2nd.util.Util;

import org.w3c.dom.Text;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static com.bit.bitcare.mcflp2nd.sign.R.style.AlertDialogCustom;

public class SignatureDialogBuilder {

    public Activity mActivity;
    public interface SignatureEventListener {
        public void onSignatureEntered(File savedFile);
        public void onSignatureEntered(File savedFile, String date, String comment);
        public void onSignatureInputCanceled();
        public void onSignatureInputError(Throwable e);
        public void onSignatureInputReset();
    }
    AlertDialog builder = null;

    private LinearLayout ll_sig__cooperation;
    private LinearLayout ll_sig__signature;
    private EditText et_dialog_date;
    private EditText et_dialog_comment;
    private TextView tv_dialog_target_name;
    private String jdgmtDocjdgmtday;
    private String jdgmtRegday;

    public void show(Activity activity, final SignatureEventListener eventListener, String title, String formCode, String formType, String targetName, String docjdgmtcont, String docjdgmtday, String regday){
        final View view = buildView(activity);
        final SignatureInputView inputView = (SignatureInputView) view.findViewById(R.id.sig__input_view);
        mActivity = activity;

        ll_sig__cooperation = view.findViewById(R.id.ll_sig__cooperation);
        ll_sig__signature = view.findViewById(R.id.ll_sig__signature);
        et_dialog_date = view.findViewById(R.id.et_dialog_date);
        et_dialog_comment = view.findViewById(R.id.et_dialog_comment);
        tv_dialog_target_name = view.findViewById(R.id.tv_dialog_target_name);
        jdgmtDocjdgmtday = docjdgmtday;
        jdgmtRegday = regday;

        if("03".equals(formCode)){ // 판단서면
            ll_sig__cooperation.setVisibility(View.VISIBLE);
            ll_sig__signature.setVisibility(View.GONE);
//            String toDate = "";
//            if("".equals(docjdgmtday)){
//                toDate = String.valueOf(Util.Companion.getToDateforDialog());
//                et_dialog_date.setText(toDate);
//            }else{
                et_dialog_date.setText(jdgmtDocjdgmtday); //판단일이 없으면 그냥 빈스트링으로 놔둠
//            }
            et_dialog_comment.setText(docjdgmtcont);
        }else{
            ll_sig__cooperation.setVisibility(View.GONE);
            ll_sig__signature.setVisibility(View.VISIBLE);
            tv_dialog_target_name.setText(targetName);
        }
        et_dialog_date.setOnClickListener(v -> {

            Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);

            Context context = new ContextThemeWrapper(activity, android.R.style.Theme_Holo_Light_Dialog);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // API 24 이상일 경우 시스템 기본 테마 사용
                context = activity;
            }
            DatePickerDialog datePickerDialog = new DatePickerDialog(context, dateSetListener, year, month, day);
            datePickerDialog.show();

        });

        builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.AlertDialogCustom))
                .setTitle(title)
                .setView(view)
                .setPositiveButton(R.string.sig__default_dialog_action_confirm, (dialogInterface, i) -> {
                    try{
                        if(!inputView.isSignatureInputAvailable())
                            throw new NoSignatureException("No signature found");

                        File saved = inputView.saveSignature();

                        if("03".equals(formCode)){ //판단서 일 때만
                            eventListener.onSignatureEntered(saved, et_dialog_date.getText().toString(), et_dialog_comment.getText().toString().trim());
//                            eventListener.onSignatureEntered(saved, et_dialog_date.getText().toString(), et_dialog_comment.getText().toString().trim());
                        }else{
                            eventListener.onSignatureEntered(saved);
                        }
                    }
                    catch(Exception e){
                        e.printStackTrace();
                        eventListener.onSignatureInputError(e);
                    }
                })
                .setNegativeButton(R.string.sig__default_dialog_action_cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        eventListener.onSignatureInputCanceled();
                    }
                })
                .setNeutralButton(R.string.sig__default_dialog_action_reset, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        eventListener.onSignatureInputReset();
//                        inputView.undoAllSignaturePath();
                    }
                })
                .show();

        builder.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(activity.getResources().getColor(R.color.colorPrimary));
        builder.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(activity.getResources().getColor(R.color.secondary_text));

        builder.getButton(AlertDialog.BUTTON_NEUTRAL).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputView.undoAllSignaturePath();

                Boolean wantToCloseDialog = false;
                //Do stuff, possibly set wantToCloseDialog to true then...
                if(wantToCloseDialog)
                    builder.dismiss();
                //else dialog stays open. Make sure you have an obvious way to close the dialog especially if you set cancellable to false.
            }
        });

        view.findViewById(R.id.sig__action_undo)
                .setOnClickListener(new View.OnClickListener() {
                    public void onClick(View view) {
                        inputView.undoLastSignaturePath();
                    }
                });
    }

    protected View buildView(Activity activity){
        return activity.getLayoutInflater().inflate(R.layout.sig__default_dialog, null, false);
    }

    public void dismiss(){
        if(builder != null){
            builder.dismiss();
        }
    }

    private DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            String compareDay = year + "-" + String.format("%02d",(monthOfYear+1)) + "-" + String.format("%02d", dayOfMonth);

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date jdgmtDocjdgmtdate = null;
            Date jdgmtRegdate = null;
            try{
                jdgmtDocjdgmtdate = sdf.parse(compareDay);
                jdgmtRegdate = sdf.parse(jdgmtRegday);
            } catch (ParseException e) {
                Log.e("dialog","날짜 비교 에러");
                throw new DateCheckException("DateCheckException");
            }
            int compare = jdgmtDocjdgmtdate.compareTo(jdgmtRegdate);
//                                if(compare > 0){
//                                    Log.e("dialog","jdgmtDocjdgmtdate > jdgmtRegdate");
//                                }else
            if(compare <= 0){
                Log.e("dialog","jdgmtDocjdgmtdate < jdgmtRegdate");
                jdgmtDocjdgmtday = compareDay;
                et_dialog_date.setText(jdgmtDocjdgmtday);
            }else{
                Log.e("dialog","jdgmtDocjdgmtdate = jdgmtRegdate");
                Toast.makeText(mActivity,"판단일을 다시 선택해 주세요", Toast.LENGTH_LONG).show();
            }//판단일이 미래일순 없다 판단일이 작거나같다
        }
    };

}
