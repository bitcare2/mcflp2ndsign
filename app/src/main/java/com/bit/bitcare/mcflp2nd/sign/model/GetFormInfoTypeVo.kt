package com.bit.bitcare.mcflp2nd.sign.model

data class GetFormInfoTypeVo(
        var signFormSN: String?,
        var formCode: String?,
        var requestCode: String?,
        var requestState: String?,
        var tempFormType: String?,
        var formSN: String?,
        var cancelType: String?
)