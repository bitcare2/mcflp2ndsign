package com.bit.bitcare.mcflp2nd.sign.ui

import android.content.Context
import android.content.Intent
import android.content.Intent.*
import android.os.Bundle
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.AttributeSet
import android.view.View

import android.content.pm.PackageManager
import android.R.attr.versionCode
import android.R.attr.versionName
import android.content.DialogInterface
import android.content.pm.PackageInfo
import android.net.Uri
import android.os.AsyncTask
import android.provider.Settings
import android.util.Log
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import android.support.v4.content.ContextCompat.startActivity
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.marcoscg.licenser.Library
import com.marcoscg.licenser.License
import com.marcoscg.licenser.LicenserDialog
import kotlinx.android.synthetic.main.activity_app_info.*
import kotlinx.android.synthetic.main.toolbar_sign.*

import org.jsoup.Jsoup
import java.io.IOException
import java.util.regex.Pattern


class AppInfoActivity : BaseActivity() {
    var pInfo: PackageInfo? = null
    var pInfoVersion: String? = ""
    var marketVersion: String? = ""


    var gsonLicense = "Copyright 2008 Google Inc.\n" +
            "\n" +
            "Licensed under the Apache License, Version 2.0 (the \"License\");\n" +
            "you may not use this file except in compliance with the License.\n" +
            "You may obtain a copy of the License at\n" +
            "\n" +
            "    http://www.apache.org/licenses/LICENSE-2.0\n" +
            "\n" +
            "Unless required by applicable law or agreed to in writing, software\n" +
            "distributed under the License is distributed on an \"AS IS\" BASIS,\n" +
            "WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n" +
            "See the License for the specific language governing permissions and\n" +
            "limitations under the License."

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_info)

        toolbar_title.text = resources.getString(R.string.action_app_info)
        toolbar_sign_button.visibility = View.GONE

        iv_toorbar_back_button.setOnClickListener {
            onBackPressed()
        }

        app_info_btn.setOnClickListener { view ->
            val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + pInfo!!.packageName))
            intent.addCategory(Intent.CATEGORY_DEFAULT)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        }

        app_info_privacy.setOnClickListener { view ->
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://lst.go.kr/infomation/privacy.do"))
            startActivity(intent)
        }

        app_info_open_source_license.setOnClickListener { view ->
            LicenserDialog(this)
                    .setTitle("Licenses")
                    .setCustomNoticeTitle("Notices for files:")
//                    .setBackgroundColor(Color.RED) // Optional
                    .setLibrary(Library("Android Support Libraries",
                            "https://developer.android.com/topic/libraries/support-library/index.html",
                            License.APACHE))
                    .setLibrary(Library("Licenser",
                            "https://github.com/marcoscgdev/Licenser",
                            License.MIT))
                    .setLibrary(Library("Jsoup",
                            "https://jsoup.org/license",
                            License.MIT))
                    .setLibrary(Library("Material-dialogs",
                            "https://raw.githubusercontent.com/afollestad/material-dialogs/master/LICENSE.md",
                            License.APACHE))
                    .setLibrary(Library("Fast-Android-Networking",
                            "https://github.com/amitshekhariitbhu/Fast-Android-Networking#license",
                            License.APACHE))
                    .setLibrary(Library("GSON",
                            "https://github.com/google/gson#license",
                            License.APACHE))

                    .setPositiveButton(android.R.string.ok, object:DialogInterface.OnClickListener {
                        override fun onClick(dialogInterface:DialogInterface, i:Int) {

                        }
                    })
                    .show()
        }
        app_info_status.setText("현재 버전을 확인하고 있습니다...")
        pInfoVersion = checkAppVersion()
        app_info_version.setText("버전 $pInfoVersion")

        comparisonAppVersion()

    }

    private fun checkAppVersion() : String {
        var versionName: String = ""
        var versionCode: Int = -1
        try {
            pInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA)

            versionName = pInfo!!.versionName
            versionCode = pInfo!!.versionCode
            Dlog.e("versionName : " + pInfo!!.versionName)
            Dlog.e("versionCode : " + pInfo!!.versionCode)

        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        return versionName
    }

    private fun comparisonAppVersion(){
        try{
            getMarketVersion().execute()
        }catch (e: Exception){
            Dlog.e("getMarketVersion().execute() error")
        }

    }

    inner class getMarketVersion() : AsyncTask<String, String, String>() {

        private var comparisonVersion: String =""

        override fun doInBackground(vararg p0: String?): String {
            try {
                val AppFromPlayStore = "https://play.google.com/store/apps/details?id=com.bit.bitcare.mcflp2nd.sign"
                val doc = Jsoup.connect(AppFromPlayStore).get()
                val Version = doc.select(".hAyfc .htlgb")
                for (i in 0..6) {
                    comparisonVersion = Version[i].text()
                    if (Pattern.matches("^[0-9]{1}.[0-9]{2}$", comparisonVersion)) {
                        break
                    }
                }
                Dlog.e("comparisonVersion : $comparisonVersion")
                return comparisonVersion

            } catch (e: IOException) {
                e.printStackTrace()
            }
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            var pi: PackageInfo? = null
            try {
                pi = pInfo
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }

            var verSion = pi!!.versionName
            marketVersion = result.toString()

            Dlog.e("marketVersion : " + marketVersion)
            Dlog.e("verSion : " + verSion)
            if(marketVersion.equals("")){
                runOnUiThread {
                    app_info_status.setText("최신 버전을 확인할 수 없습니다.")
                }
            }else{
                if (marketVersion?.compareTo(verSion!!)!! > 0) {
                    // 업데이트 필요 마켓버전이 더 높음
                    app_info_status.setText("여기를 눌러 업데이트해주십시오. ")
                    app_info_status.setOnClickListener {
                        var goMarket: Intent = Intent(Intent.ACTION_VIEW)
                        goMarket.setData(Uri.parse("market://details?id=com.bit.bitcare.mcflp2nd"))
                        startActivity(goMarket)
                    }
                } else {
                    // 업데이트 불필요
                    app_info_status.setText("현재 최신 버전입니다.")
                }
            }
        }
    }

}