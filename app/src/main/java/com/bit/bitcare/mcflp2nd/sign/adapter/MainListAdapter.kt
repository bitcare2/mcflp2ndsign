package com.bit.bitcare.mcflp2nd.sign.adapter

import android.app.Application
import android.content.Context
import android.os.Build
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.R.id.ll_main_list_item
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.MainListItemVo
import com.fasterxml.jackson.databind.ser.Serializers


class MainListAdapter internal constructor(private val mainArrayList: ArrayList<MainListItemVo>, val listener: (MainListItemVo) -> Unit ) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class MainListHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var ll_main_list_item: LinearLayout
        internal var tv_main_list_item_name: TextView
        internal var tv_main_list_item_date: TextView
        internal var tv_main_list_item_title: TextView
        internal var tv_main_list_item_type: TextView
        internal var tv_main_list_item_number: TextView
        internal var tv_main_list_item_state: TextView
        internal var v_main_list_under_bar: View

        init {
            ll_main_list_item = view.findViewById(R.id.ll_main_list_item)
            tv_main_list_item_name = view.findViewById(R.id.tv_main_list_item_name)
            tv_main_list_item_date = view.findViewById(R.id.tv_main_list_item_date)
            tv_main_list_item_title = view.findViewById(R.id.tv_main_list_item_title)
            tv_main_list_item_type = view.findViewById(R.id.tv_main_list_item_type)
            tv_main_list_item_number = view.findViewById(R.id.tv_main_list_item_number)
            tv_main_list_item_state = view.findViewById(R.id.tv_main_list_item_state)
            v_main_list_under_bar = view.findViewById(R.id.v_main_list_under_bar)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_main, parent, false)
        return MainListHolder(v)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val mainListHolder = holder as MainListHolder
        mainListHolder.tv_main_list_item_name.text = mainArrayList[position].name+" ("+mainArrayList[position].gender+")"
        mainListHolder.tv_main_list_item_date.text = mainArrayList[position].date
        mainListHolder.tv_main_list_item_title.text = mainArrayList[position].title
        mainListHolder.tv_main_list_item_type.text = mainArrayList[position].type
        mainListHolder.tv_main_list_item_number.text = mainArrayList[position].number
        mainListHolder.tv_main_list_item_state.text = mainArrayList[position].state
        mainListHolder.ll_main_list_item.setOnClickListener{ listener(mainArrayList[position]) }
        //타입에 따른 배경 변경
        when(mainArrayList[position].type!!.trim()){
            "작성자 서명 요청" -> {
                    mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_dull_orange)
            }
            "작성자 확인 서명 요청" -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_dull_orange)
            }
            "환자 서명 요청" -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_dull_orange)
            }
            "환자 가족 서명 요청" -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_darkish_pink)
            }
            "전문의 협진 요청" -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_dark_sky_blue)
            }
            "법정대리인 서명 요청" -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_darkish_green)
            }
            else -> {
                mainListHolder.tv_main_list_item_type.setBackgroundResource(R.drawable.rect_dull_orange)
            }
        }

        //홀수 배경 다르게
        if(position % 2 == 1){
            mainListHolder.ll_main_list_item.setBackgroundColor(BaseApplication.globalApplicationContext!!.resources.getColor(R.color.main_list_background))
        }

//        //마지막 줄 언더바 안보이게
//        if(position != (mainArrayList.size-1)){
//            mainListHolder.v_main_list_under_bar.visibility = View.INVISIBLE
//        }
    }

    override fun getItemCount(): Int {
        return mainArrayList.size
    }
}