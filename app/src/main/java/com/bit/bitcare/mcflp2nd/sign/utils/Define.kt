package com.bit.bitcare.mcflp2nd.sign.utils

import android.Manifest

class Define {
    companion object {
//        const val serverIP: String = "http://59.10.164.97:8080" // 비트 테스트 서버
        var serverIP: String = "https://mobile.lst.go.kr" // 실서버
//        var serverIP: String = "http://nibp-mobile.g2e.co.kr" // 지투이 테스트 서버
//        public val serverIP: String = "http://59.10.164.81:8080"

        var appSetupInfoServerIP = "https://mobile.lst.go.kr" //앱설정 API 용
        //        var serverIP = "nibp-intra.g2e.co.kr" //실서버
//        val bitLocalServerIP = "59.10.164.89:8080" //테스트서버

        //        var mobileServerIP = "nibp-intra.g2e.co.kr" //임시파일업로드 및 메뉴
//        val mobileServerIP = "59.10.164.89:8080" //임시 파일 업로드 및 메뉴

        var ozEndPoint = "https://report.lst.go.kr:443/oz70/server" //실서버
//            var ozEndPoint = "http://14.36.46.131:8480/oz70/server" //G2E 테스트 서버
//        const val ozEndPoint = "http://59.10.164.94:8080/oz70/server"

        const val REQUEST_RECORD_VOICE_PERMISSION = 1001
        const val REQUEST_RECORD_VIDEO_PERMISSION = 1002
        const val REQUEST_FILE_PERMISSION = 1003
        const val REQUEST_SELECT_FILE_LEGACY = 2001
        const val REQUEST_SELECT_FILE = 2002
        const val REQUEST_RECORD_SOUND_ACTION = 3001
        const val REQUEST_RECORD_VIDEO_CAPTURE = 3002
        const val REQUEST_SEARCH_JUSO = 4001
        const val REQUEST_REG_FORM = 4002

        val needVoiceRecordPermissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val needVideoRecordPermissions = arrayOf(Manifest.permission.RECORD_AUDIO, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        val needFilePermissions = arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE)

    }
}