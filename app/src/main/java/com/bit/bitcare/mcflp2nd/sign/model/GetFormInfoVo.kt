package com.bit.bitcare.mcflp2nd.sign.model

import java.util.*

data class GetFormInfoVo(
        var serviceCode: String,
        var serviceMsg: String,
        var token: String,
        var formType: String,
        var userInfo: String,
        var result: String
)