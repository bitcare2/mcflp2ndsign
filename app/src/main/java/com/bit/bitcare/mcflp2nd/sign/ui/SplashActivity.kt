package com.bit.bitcare.mcflp2nd.sign.ui

import android.annotation.SuppressLint
import android.content.Intent
import android.content.Intent.*
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.os.PersistableBundle
import android.provider.Settings
import android.support.annotation.NonNull
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.StringRequestListener
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo

import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.*

import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util


import com.google.android.gms.tasks.OnFailureListener
import com.google.android.gms.tasks.OnSuccessListener

import com.google.firebase.dynamiclinks.FirebaseDynamicLinks
import com.google.firebase.dynamiclinks.PendingDynamicLinkData
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.FirebaseInstanceIdReceiver
import com.google.firebase.iid.FirebaseInstanceIdService
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_certification_code.*
import kotlinx.android.synthetic.main.activity_splash.*
import org.json.JSONObject
import java.lang.Thread.sleep
import java.util.*
import kotlin.coroutines.experimental.EmptyCoroutineContext
import kotlin.math.sign

class SplashActivity : BaseActivity() {
    lateinit var pInfo: PackageInfo
    private var handler = Handler()
    private var token: String? = null
    private var androidID = ""
    private var i: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
    }

    override fun onResume() {
        super.onResume()
        getAppSetupInfo()
    }

    fun getAppSetupInfo(){
        Dlog.e("getAppSetupInfo")
        AndroidNetworking.post(Define.appSetupInfoServerIP+"/temp/setup")
                .addQueryParameter("appClsCode","01")
                .addQueryParameter("newYn","Y")
                .addQueryParameter("appOperSysMfg","1")
                .setPriority(Priority.HIGH)
                .build()
                .getAsString(stringRequestListener)
    }

    val stringRequestListener = object: StringRequestListener {
        override fun onResponse(response:String) {
            Dlog.e("stringRequestListener : " + response)

            var appSetupInfoVo: AppSetupInfoVo = gson.fromJson(response, AppSetupInfoVo::class.java)
            Dlog.e("appSetupInfoVo : " + appSetupInfoVo.toString())

            var serverIp = ""

            when(appSetupInfoVo.appOpfg){
                "1" -> { //운영서버
                    serverIp = "https://mobile.lst.go.kr"
                    PreferenceUtil(this@SplashActivity).putStringExtra("serverIP", "https://mobile.lst.go.kr")
                    Dlog.e("https://mobile.lst.go.kr 세팅됨")
                }
                "2" -> { //G2E테스트서버
                    serverIp = "http://nibp-mobile.g2e.co.kr"
                    PreferenceUtil(this@SplashActivity).putStringExtra("serverIP", "http://nibp-mobile.g2e.co.kr")
                    Dlog.e("http://nibp-mobile.g2e.co.kr 세팅됨")
                }
            }

            Define.serverIP = serverIp


//            Define.serverIP = "http://nibp-mobile.g2e.co.kr"
//            Define.ozEndPoint = "http://nibp-report.g2e.co.kr/oz70/server"


            //최소버전 먼저 체크 후 비어 있으면 공지사항 확인

            //// for Test
//            appSetupInfoVo.appMummVer = "0.20"
//            appSetupInfoVo.appOpMsg = "공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지공지사항메세지입니다. 이렇게 길면 어떻게 나오=는지지"

            when(appSetupInfoVo.appMummVer){
                "" -> {
                    checkNotice(appSetupInfoVo.appOpMsg!!)
                }
                else -> {
                    var versionName: String = ""

                    pInfo = packageManager.getPackageInfo(packageName, PackageManager.GET_META_DATA)

                    versionName = pInfo!!.versionName
                    Dlog.e("versionName.toLong() : " + versionName.toFloat())
                    Dlog.e("appSetupInfoVo.appMummVer!!.toLong() : " + appSetupInfoVo.appMummVer!!.toFloat() )

                    if(appSetupInfoVo.appMummVer!!.toFloat() > versionName.toFloat()){
                        MaterialDialog(this@SplashActivity)
                                .title(R.string.update_check)
                                .message(R.string.update_check_message)
                                .positiveButton (R.string.move){
                                    var goMarket: Intent = Intent(Intent.ACTION_VIEW)
                                    goMarket.setData(Uri.parse("market://details?id=com.bit.bitcare.mcflp2nd.sign"))
                                    startActivity(goMarket)
                                }
                                .negativeButton(R.string.close){

                                }
                                .show{
                                    setCancelable(false)
                                    setCanceledOnTouchOutside(false)
                                }
                    }else{
                        checkNotice(appSetupInfoVo.appOpMsg!!)
                    }
                }
            }
        }

        override fun onError(error: ANError) {
            if (error.errorCode !== 0) {
                // received error from server
                // error.getErrorCode() - the error code from server
                // error.getErrorBody() - the error body from server
                // error.getErrorDetail() - just an error detail
                Dlog.d("onError errorCode : " + error.errorCode)
                Dlog.d("onError errorBody : " + error.errorBody)
                Dlog.d("onError errorDetail : " + error.errorDetail)
            } else {
                // error.getErrorDetail() : connectionError, parseError, requestCancelledError
                Dlog.d("onError errorDetail : " + error.errorDetail)
            }
        }
    }

    private fun checkNotice(noticeString: String){
        when(noticeString){
            "" -> {
                checkDeepLink()
            }
            else -> {
                MaterialDialog(this@SplashActivity)
                        .title(R.string.notice)
                        .positiveButton (R.string.done){
                            checkDeepLink()
                        }
                        .show{
                            message(text = noticeString)
                        }
            }
        }
    }

    private fun checkDeepLink(){
        if ("Y".equals(PreferenceUtil(this@SplashActivity).getStringExtra("isRegisted"))) {
            handleDeepLink()
        } else {
            handler = @SuppressLint("HandlerLeak")
            object : Handler() {
                override fun handleMessage(msg: Message) {
                    if (i === 10) {
                        this.removeMessages(0) // 반복실행되는 핸들러를 멈춰주는 함수.
                    } else {
                        this.sendEmptyMessageDelayed(0, 1000) // ms단위 딜레이 함수
                        makeToken()
                        i++
                    }
                }
            }
            handler.sendEmptyMessage(0)
        }
    }

    private fun makeToken() {
        token = FirebaseInstanceId.getInstance().token
        if (token != null) {
            Dlog.e("token = $token")
            handler.removeMessages(0)
            requestInputDeviceInfo()
        } else {
            Dlog.e("token is null")
        }
    }

    private fun requestInputDeviceInfo() {
        androidID = Settings.Secure.getString(baseContext.contentResolver, Settings.Secure.ANDROID_ID)
        PreferenceUtil(this).putStringExtra("androidID", androidID)
        Dlog.e("androidID = $androidID")

        Dlog.e("seok Define.serverIP/sign/addDeviceInfo: " + Define.serverIP + "/sign/addDeviceInfo")

        AndroidNetworking.post(Define.serverIP + "/sign/addDeviceInfo")
                .addBodyParameter("deviceSID", androidID)
                .addBodyParameter("deviceRegID", token)
                .addBodyParameter("deviceType", "2") // OS타입(1 : 아이폰, 2 : 안드로이드)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("requestInputDeviceInfo 받아옴??")
                        Dlog.e(response.toString())
                        var responseVo = gson.fromJson(response.toString(), ResponseVo::class.java)
                        Dlog.e(responseVo.toString())
                        if (responseVo.serviceCode == 100 || responseVo.serviceCode == 102) { //100:성공 & 102:중복
                            PreferenceUtil(this@SplashActivity).putStringExtra("isRegisted", "Y")
                            handleDeepLink()
                        } else {
                            reTryDialog()
                        }
                    }

                    override fun onError(error: ANError) {
                        Dlog.e("requestInputDeviceInfo 에러??")
                        reTryDialog()
                    }
                })
    }

    private fun reTryDialog() {
        MaterialDialog(this@SplashActivity)
                .title(R.string.error)
                .message(R.string.re_try_message)
                .positiveButton(R.string.re_try) {
                    handler.postDelayed({ makeToken() }, 1000)
                }
                .show()
    }

    private fun handleDeepLink() {
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, object : OnSuccessListener<PendingDynamicLinkData> {
                    override fun onSuccess(pendingDynamicLinkData: PendingDynamicLinkData?) {
                        if (pendingDynamicLinkData == null) {
                            Dlog.e("다이나믹 링크가 아닌 그냥 진입")
                            if("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))){
                                Dlog.e("userSN 없음")
                                val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                                intent.addFlags(FLAG_ACTIVITY_NO_ANIMATION)
                                intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                startActivity(intent)
                                overridePendingTransition(0, 0)
                                finish()
                            }else{
                                Dlog.e("userSN 있음")
                                alreadyAuthenticatedUsers()
                            }
                        } else {
                            Dlog.e("다이나믹 링크가 있음")
                            val deepLink = pendingDynamicLinkData?.getLink()
                            Dlog.e("deepLink: $deepLink")
                            val segment = deepLink?.lastPathSegment
                            Dlog.e("segment: $segment")

                            //ex) https://mcflp2nd.page.link/?link=https://nibp.go.kr/nibp?code%3DSV3HBD4&apn=com.bit.bitcare.mcflp2nd.sign
                            //    https://mcflp2nd.page.link/?link=https://nibp.go.kr/nibp?signFormSN%3D777&apn=com.bit.bitcare.mcflp2nd.sign
                            when (segment) {
                                "nibp" -> {
                                    val code = deepLink.getQueryParameter("code")
                                    Dlog.e("code: " + code)

                                    val signFormSN = deepLink.getQueryParameter("signFormSN")

                                    if(signFormSN == null){
                                        checkCertificationCodeForNoSignForm(code)
                                    }else{
                                        checkCertificationCode(code, signFormSN)
                                    }
                                }
                                null -> {
                                    Dlog.e("segment null")

                                }
                            }
                        }
                    }
                })
                .addOnFailureListener(this, object : OnFailureListener {
                    override fun onFailure(e: Exception) {
                        Dlog.e("getDynamicLink:onFailure :$e")
                    }
                })
    }

    fun alreadyAuthenticatedUsers() {
        token = FirebaseInstanceId.getInstance().token
        if (token != null) {
            Dlog.e("token = $token")
        } else {
            Dlog.e("token is null")
            MaterialDialog(this)
                    .title(R.string.fail_create_token)
                    .message(R.string.fail_create_token_message)
                    .positiveButton(R.string.restart) {
                        recreate()
                    }
                    .negativeButton(R.string.no) {
                    }
                    .show()
        }
        AndroidNetworking.post(Define.serverIP + "/sign/autoLogin")
                .addBodyParameter("token", PreferenceUtil(this@SplashActivity).getStringExtra("token"))
                .addBodyParameter("userSN", PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))
                .addBodyParameter("deviceSID", PreferenceUtil(this@SplashActivity).getStringExtra("androidID"))
                .addBodyParameter("deviceRegID", token)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("alreadyAuthenticatedUsers 자동 로그인 성공")
                        Dlog.e(response.toString())

                        var signLoginVo = gson.fromJson(response.toString(), SignLoginVo::class.java)
                        if("100".equals(signLoginVo.serviceCode)) {
                                    Dlog.e("signLoginVo.toString() : " + signLoginVo.toString())
                                    Dlog.e("signLoginVo.result.toString() : " + signLoginVo.result.toString())
                                    Dlog.e("gson.toJson(signLoginVo.token : " + gson.toJson(signLoginVo.token).replace("\"", ""))
                                    Dlog.e("signLoginVo.result.userSN : " + signLoginVo.result.userSN)

                                    PreferenceUtil(this@SplashActivity).putStringExtra("token", gson.toJson(signLoginVo.token).replace("\"", ""))
                                    PreferenceUtil(this@SplashActivity).putStringExtra("signLoginVo", gson.toJson(signLoginVo))
                                    PreferenceUtil(this@SplashActivity).putStringExtra("signLoginResultVo", gson.toJson(signLoginVo.result))
                                    PreferenceUtil(this@SplashActivity).putStringExtra("userSN", signLoginVo.result.userSN)
                                    PreferenceUtil(this@SplashActivity).putStringExtra("userName", signLoginVo.result.userName)
                                    PreferenceUtil(this@SplashActivity).putStringExtra("orgName", signLoginVo.result.orgName)

                                    val intent = Intent(this@SplashActivity, MainActivity::class.java)
//                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                        } else {
                            Dlog.e("serviceCode 100이 아님")
                            Util.showToast("자동 로그인 실패")
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("alreadyAuthenticatedUsers 자동 로그인 실패")
                        Util.showToast("자동 로그인 실패")
                        PreferenceUtil(this@SplashActivity).clear()
                        handler.postDelayed({ makeToken() }, 1000)
                    }
                })
    }

    fun getFormInfo(signFormSN: String){
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("getFormInfo token : " + token)

        AndroidNetworking.post(Define.serverIP+"/sign/getFormInfo")
                .addBodyParameter("token", token)
                .addBodyParameter("signFormSN", signFormSN)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("getFormInfo 받아옴")
                        Dlog.e(response.toString())

                        var getFormInfo: LinkedTreeMap<String, Any> = gson.fromJson(response.toString(), object : TypeToken<LinkedTreeMap<String, Any>>() {}.type)

                        Dlog.e(getFormInfo.get("serviceCode").toString())

                        if("100.0".equals(getFormInfo.get("serviceCode").toString()) || "100".equals(getFormInfo.get("serviceCode").toString())) {

                            Dlog.e("getFormInfo.get(\"token\") : " + getFormInfo.get("token") as String)
                            PreferenceUtil(this@SplashActivity).putStringExtra("token", getFormInfo.get("token") as String)

                            var getFormInfoTypeVo: GetFormInfoTypeVo = gson.fromJson(gson.toJson(getFormInfo.get("formType")), GetFormInfoTypeVo::class.java)
                            Dlog.e("getFormInfoTypeVo : " + getFormInfoTypeVo.toString())

                            PreferenceUtil(this@SplashActivity).putStringExtra("signFormSN", getFormInfoTypeVo.signFormSN)
                            Dlog.e("getFormInfoTypeVo.signFormSN : " + getFormInfoTypeVo.signFormSN)

                            var userInfo: UserInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("userInfo")), UserInfoVO::class.java)
                            Dlog.e("userInfo : " + userInfo.toString())

                            var bundle = Util.classifyFormType(getFormInfo, getFormInfoTypeVo, userInfo, gson)

                            if (bundle.getString("ozFileName").equals("잘못된 서식요청 코드") || bundle.getString("ozFileName").equals("잘못된 서식요청") || bundle.getString("ozFileName").equals("결과값 없음") || bundle.getString("ozFileName").equals("데이터 파싱 에러")) {
                                Util.showToast(bundle.getString("ozFileName"))
                            } else {
                                bundle.putString("formInfo", gson.toJson(getFormInfoTypeVo))
                                goToForm(bundle)
                            }
                        }else{
                            Toast.makeText(this@SplashActivity, getFormInfo.get("serviceMsg") as String,Toast.LENGTH_LONG).show()
                        }

                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("getFormInfo error.errorBody" + error.errorBody)
                        Dlog.e("getFormInfo error.errorDetail" + error.errorDetail)
                        Util.showToast("올바른 번호를 입력해주세요.")
                    }
                })
    }

    fun goToForm(bundle: Bundle) {
        if ("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))) {
            val intent = Intent(this, EFormViewerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(0, 0)
            finish()
        } else {  //userSN 이 있으면 자동로그인 -> 메인 깔고 폼으로 이동
            val intent = Intent(this, MainActivity::class.java)
            val intent2 = Intent(this, EFormViewerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            overridePendingTransition(0, 0)
            intent2.putExtras(bundle)
            startActivity(intent2)
            finish()
        }
    }

    private fun checkCertificationCode(certificationCode: String, signFormSN: String){
        val fcmToken = FirebaseInstanceId.getInstance().token
        val loginType = Util.checkLoginType(certificationCode)
        val androidID = Settings.Secure.getString(baseContext.contentResolver, Settings.Secure.ANDROID_ID)

        AndroidNetworking.post(Define.serverIP+"/sign/checkLoginkey")
                .addBodyParameter("deviceType", "2") // OS타입(1 : 아이폰, 2 : 안드로이드)
                .addBodyParameter("loginType", loginType) //  03: 웹사이트인증 C, 04: 서식서명인증 S, 05: 협진인증 A
                .addBodyParameter("loginKey", certificationCode)
                .addBodyParameter("deviceSID", androidID)
                .addBodyParameter("deviceRegID", fcmToken)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("받아옴??")
                        Dlog.e(response.toString())
                        var checkLoginKeyVo = gson.fromJson(response.toString(), CheckLoginKeyVo::class.java)

                        if("100".equals(checkLoginKeyVo.serviceCode)) {
                            Dlog.e(checkLoginKeyVo.toString())
                            Dlog.e(checkLoginKeyVo.result.toString())

                            if(checkLoginKeyVo.result.userSN !=null){
                                Dlog.e("checkLoginKeyVo.result.userSN !=null")
                                Dlog.e("checkLoginKeyVo.result.userSN : ${checkLoginKeyVo.result.userSN}")
                                Dlog.e("PreferenceUtil(this@SplashActivity).getStringExtra(\"userSN\")) : "+PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))
                                if (checkLoginKeyVo.result.userSN.equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))) { //userSN 같으면 바로 서식 띄움
                                    Dlog.e("signFormSN: $signFormSN")
                                    Dlog.e("token: $token")
                                    getFormInfo(signFormSN)
                                } else {
                                    Dlog.e("userSN 같지 않음!")
                                    if("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))){
                                        Dlog.e("userSN 없음")
                                        val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                        intent.putExtra("code", certificationCode)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()
                                    }else{
                                        val intent = Intent(this@SplashActivity, SplashDialogActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                        intent.putExtra("code", certificationCode)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()
                                    }
                                }
                            }else{ //userSN이 NULL 일때 - 비회원
                                Dlog.e("userSN이 NULL임!")
                                if("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))){
                                    Dlog.e("userSN 없음")
                                    val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                    intent.putExtra("code", certificationCode)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                }else {
                                    val intent = Intent(this@SplashActivity, SplashDialogActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                    intent.putExtra("code", certificationCode)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                }
                            }
                        }else{
                            Toast.makeText(this@SplashActivity, checkLoginKeyVo.serviceMsg, Toast.LENGTH_LONG).show()

                            val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                            intent.putExtra("code", certificationCode)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            finish()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("에러??")
                        Toast.makeText(this@SplashActivity,"SMS 메세지를 확인하시고\n올바른 인증 번호를 입력해 주세요.",Toast.LENGTH_LONG).show()

                        val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        finish()
                    }
                })

    }

    private fun checkCertificationCodeForNoSignForm(certificationCode: String){
        Dlog.e("checkCertificationCodeForNoSignForm")
        val fcmToken = FirebaseInstanceId.getInstance().token
        val loginType = Util.checkLoginType(certificationCode)
        val androidID = Settings.Secure.getString(baseContext.contentResolver, Settings.Secure.ANDROID_ID)

        AndroidNetworking.post(Define.serverIP+"/sign/checkLoginkey")
                .addBodyParameter("deviceType", "2") // OS타입(1 : 아이폰, 2 : 안드로이드)
                .addBodyParameter("loginType", loginType) //  03: 웹사이트인증 C, 04: 서식서명인증 S, 05: 협진인증 A
                .addBodyParameter("loginKey", certificationCode)
                .addBodyParameter("deviceSID", androidID)
                .addBodyParameter("deviceRegID", fcmToken)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("받아옴??")
                        Dlog.e(response.toString())
                        var checkLoginKeyVo = gson.fromJson(response.toString(), CheckLoginKeyVo::class.java)

                        if("100".equals(checkLoginKeyVo.serviceCode)) {
                            Dlog.e(checkLoginKeyVo.toString())
                            Dlog.e(checkLoginKeyVo.result.toString())

                            if(checkLoginKeyVo.result.userSN !=null){
                                Dlog.e("checkLoginKeyVo.result.userSN !=null")
                                Dlog.e("checkLoginKeyVo.result.userSN : ${checkLoginKeyVo.result.userSN}")
                                Dlog.e("PreferenceUtil(this@SplashActivity).getStringExtra(\"userSN\")) : "+PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))
                                if (checkLoginKeyVo.result.userSN.equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))) { //userSN 같으면 바로 서식 띄움
                                    Dlog.e("token: $token")
                                    //Main 으로 이동
                                    alreadyAuthenticatedUsers()
                                } else {
                                    Dlog.e("userSN 같지 않음! - signFormSN도 없음")
                                    if("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))){
                                        Dlog.e("userSN 없음")
                                        val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                        intent.putExtra("code", certificationCode)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()
                                    }else {
                                        val intent = Intent(this@SplashActivity, SplashDialogActivity::class.java)
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                        intent.putExtra("code", certificationCode)
                                        startActivity(intent)
                                        overridePendingTransition(0, 0)
                                        finish()
                                    }
                                }
                            }else{ //userSN이 NULL 일때 - 비회원임
                                Dlog.e("userSN이 NULL임!")
                                if("".equals(PreferenceUtil(this@SplashActivity).getStringExtra("userSN"))){
                                    Dlog.e("userSN 없음")
                                    val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                    intent.putExtra("code", certificationCode)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                }else {
                                    val intent = Intent(this@SplashActivity, SplashDialogActivity::class.java)
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                    intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                                    intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                                    intent.putExtra("code", certificationCode)
                                    startActivity(intent)
                                    overridePendingTransition(0, 0)
                                    finish()
                                }
                            }
                        }else{
                            Toast.makeText(this@SplashActivity, checkLoginKeyVo.serviceMsg, Toast.LENGTH_LONG).show()

                            val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                            intent.putExtra("code", certificationCode)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                            finish()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("에러??")
                        Toast.makeText(this@SplashActivity,"SMS 메세지를 확인하시고\n올바른 인증 번호를 입력해 주세요.",Toast.LENGTH_LONG).show()

                        val intent = Intent(this@SplashActivity, CertificationCodeActivity::class.java)
                        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                        intent.addFlags(FLAG_ACTIVITY_CLEAR_TASK)
                        intent.addFlags(FLAG_ACTIVITY_NEW_TASK)
                        startActivity(intent)
                        overridePendingTransition(0, 0)
                        finish()
                    }
                })
    }

}

