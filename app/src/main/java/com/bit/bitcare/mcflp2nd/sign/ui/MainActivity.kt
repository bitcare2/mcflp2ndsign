package com.bit.bitcare.mcflp2nd.sign.ui

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.adapter.MainListAdapter
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.widget.AdapterView.OnItemClickListener
import com.afollestad.materialdialogs.MaterialDialog
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import android.support.annotation.NonNull
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.utils.quick.ActionItem
import com.bit.bitcare.mcflp2nd.sign.utils.quick.QuickAction
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.toolbar_sign.*
import java.util.*
import android.support.v4.view.MenuItemCompat.getActionView
import android.support.v7.widget.RecyclerView
import android.widget.ImageButton
import android.widget.Toast
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.sign.R.id.action_filter
import com.bit.bitcare.mcflp2nd.sign.R.id.rv_main
import com.bit.bitcare.mcflp2nd.sign.model.*
import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.util.Util
import com.google.gson.internal.LinkedTreeMap
import org.json.JSONObject
import kotlin.math.absoluteValue


class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {
    companion object {
        private var yPosition: Int = 0
    }
    private var mainListData: ArrayList<MainListItemVo>? = null
    private var mainListData1: ArrayList<MainListItemVo>? = null
    private var mainListData2: ArrayList<MainListItemVo>? = null
    private var mainListAdapter: MainListAdapter? = null
    private var dataKey: String? = ""
    private var listData: String? = ""
    private var isSignWaitList = true // 서명 대기 목록 or 완료 목록
    private var period: Int = -1 // 검색 기간
    private var mLayoutManager: LinearLayoutManager? = null
    private var viewOffset = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        toolbar.setTitleTextAppearance(this, R.style.ToolbarMain)

//        dataKey = intent.getStringExtra("dataKey")

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.menu.getItem(2).setCheckable(false) //정보
        nav_view.menu.getItem(3).setCheckable(false) //로그아웃

//        사용자 정보
        var userName = PreferenceUtil(this@MainActivity).getStringExtra("userName")
        var orgName = PreferenceUtil(this@MainActivity).getStringExtra("orgName")

        //서랍 메뉴 헤더 세팅
        val drawerTitle = nav_view.getHeaderView(0).findViewById<TextView>(R.id.tv_nav_header_title)
        drawerTitle.setText(userName)
        val drawerSubTitle = nav_view.getHeaderView(0).findViewById<TextView>(R.id.tv_nav_header_subtitle)
        drawerSubTitle.setText(orgName)

        // 레이아웃 매니저
        mLayoutManager = LinearLayoutManager(this)
        rv_main.setLayoutManager(mLayoutManager)

        nav_view.menu.getItem(0).isChecked = true
        isSignWaitList = true
    }

    override fun onResume() {
        super.onResume()
        Dlog.e("onResume()")
        dataKey = ""
        listData = ""
        getFormList(period, "")
    }

    fun setList() {
        Dlog.e("setList()")
        try{
            Dlog.e("dataKey : $dataKey")
            Dlog.e("listData : $listData")

            if ("".equals(dataKey) || dataKey == null) {
                Dlog.e("in setList - if dataKey : $dataKey")
                getFormList(period, "")
            }else{
                listData = BaseApplication.instance!!.popDataHolder(dataKey) as String
                Dlog.e("listData : $listData")

                var getFormList: List<GetFormListItemVo> = gson.fromJson<List<GetFormListItemVo>>(listData, object : TypeToken<List<GetFormListItemVo>>() {}.type)
                Collections.reverse(getFormList)
                Dlog.e("getFormList Reversed : "+getFormList.toString())

                mainListData = ArrayList<MainListItemVo>()
                mainListData1 = ArrayList<MainListItemVo>()
                mainListData2 = ArrayList<MainListItemVo>()

                for (item: GetFormListItemVo in getFormList!!) {
                    Dlog.e(item.toString())

                    if ("서명대기".equals(item.requestStateName) || "최종확인대기".equals(item.requestStateName)) {
                        mainListData1?.add(MainListItemVo(item.userName, item.userGender, item.requestDate.substring(2,10), item.formName, item.requestName, "No:"+item.formNumber, item.requestStateName, item.signFormSN, item.formSN))
                    } else if ("등록완료".equals(item.requestStateName)) {
                        mainListData2?.add(MainListItemVo(item.userName, item.userGender, item.requestDate.substring(2,10), item.formName, item.requestName, "No:"+item.formNumber, item.requestStateName, item.signFormSN, item.formSN))
                    }
                }
                supportActionBar!!.title = "서명 대기 목록 ("+mainListData1!!.size+")"
                setListAdapter()
            }

        }catch (e: Exception){
            Dlog.e("setList() Exception")
            getFormList(period, "")
        }finally {
            Dlog.e("setList() finally")
            dataKey = null
            listData = null
            if (yPosition!= -1) {
                mLayoutManager!!.scrollToPositionWithOffset(yPosition, viewOffset)
            }
        }
    }

    private fun setListAdapter() {
        Dlog.e("setListAdapter()")
        Dlog.e("isSignWaitList : $isSignWaitList")
        if (isSignWaitList) {
            mainListData = mainListData1
            supportActionBar!!.title = "서명 대기 목록 ("+mainListData1!!.size+")"
        } else {
            mainListData = mainListData2
            supportActionBar!!.title = "서명 완료 목록 ("+mainListData2!!.size+")"
        }

        // 아답터
        mainListAdapter = MainListAdapter(mainListData!!) { mainListItemVo ->
            Dlog.e("${mainListItemVo.name}")
            Dlog.e("${mainListItemVo.type}")
            Dlog.e("${mainListItemVo.title}")
            Dlog.e("${mainListItemVo.date}")

            PreferenceUtil(this@MainActivity).putStringExtra("signFormSN", mainListItemVo.signFormSN)
            Dlog.e("signFormSN : ${mainListItemVo.signFormSN}")
            getFormInfo(mainListItemVo.signFormSN!!)

        }
        rv_main.setAdapter(mainListAdapter)

        Dlog.e("mainListData!!.size : " + mainListData!!.size)

        if (mainListData!!.size == 0) {
            rv_main.visibility = View.GONE
            ll_main.visibility = View.VISIBLE
        } else {
            rv_main.visibility = View.VISIBLE
            ll_main.visibility = View.GONE
        }
//        mainListAdapter!!.notifyDataSetChanged()
    }


    override fun onPause() {
        super.onPause()
        yPosition = mLayoutManager!!.findFirstVisibleItemPosition()
        val startView = rv_main.getChildAt(0)
        viewOffset = if (startView == null) 0 else startView!!.top - rv_main.paddingTop
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            finishApp()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // 왼쪽 서랍 메뉴
        when (item.itemId) {
            R.id.nav_wait -> {
                isSignWaitList = true
                setListAdapter()
            }
            R.id.nav_complete -> {
                isSignWaitList = false
                setListAdapter()
            }
            R.id.nav_information -> {
                val intent = Intent(this, AppInfoActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                startActivity(intent)
                overridePendingTransition(0, 0)
            }
            R.id.nav_logout -> {
                MaterialDialog(this)
                        .title(R.string.do_logout_title)
                        .message(R.string.do_logout_message)
                        .positiveButton(R.string.yes) {
                            Dlog.e("로그아웃 - 예")

                            PreferenceUtil(this).clear()

                            val intent = Intent(this, CertificationCodeActivity::class.java)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                            startActivity(intent)
                            overridePendingTransition(0, 0)
                        }
                        .negativeButton(R.string.no) {
                            Dlog.e("로그아웃 - 아니오")
                        }
                        .show()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    private fun finishApp() {
        MaterialDialog(this)
                .title(R.string.do_exit_title)
                .message(R.string.do_exit_message)
                .positiveButton(R.string.yes) {
                    Dlog.e("종료 - 예")
                    finish()
                }
                .negativeButton(R.string.no) {
                    Dlog.e("종료 - 아니오")
                }
                .show()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        return super.onCreateOptionsMenu(menu)
        var menuInflater: MenuInflater = menuInflater
        menuInflater.inflate(R.menu.main, menu)
        return true //true면 여기서 끝 false면 리턴 후 계속 동작
    }

    private val onActionItemClickListener = QuickAction.OnActionItemClickListener { source, pos, actionId ->
        Dlog.e("기간선택 OnActionItemClickListener")

        when (actionId) {
            0 -> {
                Dlog.e("1개월")
                getFormList(-1, "1개월간 목록 조회")
                period = -1
            }
            1 -> {
                Dlog.e("3개월")
                getFormList(-3, "3개월간 목록 조회")
                period = -3
            }
            2 -> {
                Dlog.e("6개월")
                getFormList(-6, "6개월간 목록 조회")
                period = -6
            }
            3 -> {
                Dlog.e("12개월")
                getFormList(-12, "12개월간 목록 조회")
                period = -12
            }
            4 -> {
                Dlog.e("전체")
                getFormList(-1200, "전체 목록 조회")
                period = -1200
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        return super.onOptionsItemSelected(item)
        return when (item!!.itemId) {
            R.id.action_filter -> {
                val quickAction = QuickAction(this@MainActivity, QuickAction.VERTICAL)
                val actionItem1 = ActionItem(0, "1개월", null)
                val actionItem2 = ActionItem(1, "3개월", null)
                val actionItem3 = ActionItem(2, "6개월", null)
                val actionItem4 = ActionItem(3, "12개월", null)
                val actionItem5 = ActionItem(4, "전체", null)

                quickAction.addActionItem(actionItem1)
                quickAction.addActionItem(actionItem2)
                quickAction.addActionItem(actionItem3)
                quickAction.addActionItem(actionItem4)
                quickAction.addActionItem(actionItem5)
                quickAction.setOnActionItemClickListener(onActionItemClickListener)

                var filter: View = this.findViewById(R.id.action_filter)
                quickAction.show(filter)

                false
            }
            R.id.action_refresh -> {
                yPosition = 0
                getFormList(period, "목록이 새로고침 되었습니다.")
                false
            }
            else -> {
                super.onOptionsItemSelected(item)
            }

        }
    }

    private fun getFormList(month: Int, showRefreshMessage: String) {
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("getFormList token : $token")

        AndroidNetworking.post(Define.serverIP + "/sign/getFormList")
                .addBodyParameter("token", token)
                .addBodyParameter("startDate", Util.getCalculatePeriod(month))
                .addBodyParameter("endDate", Util.getCalculatePeriod(0))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("getFormList 받아옴")
                        Dlog.e(response.toString())

                        var getFormListVo = gson.fromJson(response.toString(), GetFormListVo::class.java)
                        Dlog.e(getFormListVo.toString())

                        PreferenceUtil(this@MainActivity).putStringExtra("token", getFormListVo.token.toString().replace("\"", ""))

                        Dlog.e("getFormListVo.token.toString() : " + getFormListVo.token.toString())

                        dataKey = BaseApplication.instance!!.putDataHolder(gson.toJson(getFormListVo.result))

                        setList()

                        if(!"".equals(showRefreshMessage)){
                            Toast.makeText(this@MainActivity, showRefreshMessage, Toast.LENGTH_SHORT).show()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("getFormList 에러")
                        Dlog.e("error.errorCode "+error.errorCode)
                        Dlog.e("error.errorBody"+error.errorBody)
                        Dlog.e("error.errorDetail"+error.errorDetail)
                        Toast.makeText(this@MainActivity, "리스트 데이터 받아오기에 실패했습니다.", Toast.LENGTH_LONG).show()
                    }
                })
    }

    private fun getFormInfo(signFormSN: String) {
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("getFormInfo token : $token")
//        var signLoginResultVo = gson.fromJson(PreferenceUtil(this).getStringExtra("signLoginResultVo"), SignLoginResultVo::class.java)
//        Dlog.e("signLoginResultVo.signFormSN : " + signLoginResultVo.signFormSN)

        AndroidNetworking.post(Define.serverIP + "/sign/getFormInfo")
                .addBodyParameter("token", token)
                .addBodyParameter("signFormSN", signFormSN)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("getFormInfo 받아옴??")
                        Dlog.e(response.toString())

                        var getFormInfo: LinkedTreeMap<String, Any> = gson.fromJson(response.toString(), object : TypeToken<LinkedTreeMap<String, Any>>() {}.type)

                        if(getFormInfo.get("serviceCode") as Double == 100.0 ){
                            Dlog.e("getFormInfo.get(\"token\") : " + getFormInfo.get("token") as String)
                            PreferenceUtil(this@MainActivity).putStringExtra("token", getFormInfo.get("token") as String)

                            var getFormInfoTypeVo: GetFormInfoTypeVo = gson.fromJson(gson.toJson(getFormInfo.get("formType")), GetFormInfoTypeVo::class.java)
                            Dlog.e("getFormInfoTypeVo : " + getFormInfoTypeVo.toString())

                            var userInfo: UserInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("userInfo")), UserInfoVO::class.java)
                            Dlog.e("userInfo : " + userInfo.toString())

                            var bundle = Util.classifyFormType(getFormInfo, getFormInfoTypeVo, userInfo, gson)


                            if(bundle.getString("ozFileName").equals("잘못된 서식요청 코드") || bundle.getString("ozFileName").equals("잘못된 서식요청") || bundle.getString("ozFileName").equals("결과값 없음")
                                    || bundle.getString("ozFileName").equals("데이터 파싱 에러")){
                                Toast.makeText(this@MainActivity, bundle.getString("ozFileName"), Toast.LENGTH_LONG).show()
                            }else{
                                bundle.putString("formInfo", gson.toJson(getFormInfoTypeVo))
                                val intent = Intent(this@MainActivity, EFormViewerActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                intent.putExtras(bundle)
                                startActivity(intent)
                                overridePendingTransition(0, 0)
                            }
                        }else{
                            Toast.makeText(this@MainActivity, "서버 통신 에러.", Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("error.errorCode "+error.errorCode)
                        Dlog.e("error.errorBody"+error.errorBody)
                        Dlog.e("error.errorDetail"+error.errorDetail)
                        Toast.makeText(this@MainActivity, "서식 데이터 받아오기에 실패했습니다.", Toast.LENGTH_LONG).show()
                    }
                })
    }
}
//          그냥 참고용
//    fun divideTitle(dividedTitle: String) = when(dividedTitle){
//        "01"-> "연명의료 계획서"
//        "02"-> "사전연명의료 의향서"
//        "03"-> "임종과정에 있는 환자 판단서"
//        "04"-> "환자의사 확인서(사전연명의료의향서)"
//        "05"-> "환자가족 진술서"
//        "06"-> "친권자 및 환자가족 의사 확인서"
//        "07"-> "이행서"
//        else -> "구분못함"
//    }
//
//    fun divideType(dividedType: String) = when(dividedType){
//        "01"->"작성자 서명 요청"
//        "02"->"환자 서명 요청"
//        "03"->"환자 가족 서명 요청"
//        "04"->"전문의 협진 요청"
//        "05"->"법정대리인 서명 요청 "
//        else-> "구분못함"
//    }
//
//    fun divideState(dividedState: String) = when(dividedState){
//        "01"->"서명대기"
//        "02"->"최종확인대기"
//        "03"->"서명거부"
//        "04"->"등록완료"
//        "99"->"서명 요청 취소"
//        else-> "구분못함"
//    }

