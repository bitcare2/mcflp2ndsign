package com.bit.bitcare.mcflp2nd.sign.model


data class EdffamconfrTempDetailInfo(
    var edffamconfrDetailInfo: EdffamconfrTempDetailInfoVO?,
    var signList: List<BasicEdffamconfrSignTempInfoVO>
)