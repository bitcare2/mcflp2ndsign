package com.bit.bitcare.mcflp2nd.util

import android.os.Bundle
import android.widget.Toast
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.*
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

open class Util {
    companion object {
        fun getToDate(): String {
            var today: Date = Date()
            var simpleDateFormat: SimpleDateFormat = SimpleDateFormat("yyyyMMdd")
            return simpleDateFormat.format(today)
        }

        fun getFileSize(size: String): String {
            val gubn = arrayOf("Byte", "KB", "MB")
            var returnSize = String()
            var gubnKey = 0
            var changeSize = 0.0
            var fileSize: Long = 0
            try {
                fileSize = java.lang.Long.parseLong(size)
                var x = 0
                while (fileSize / 1024.toDouble() > 0) {
                    gubnKey = x
                    changeSize = fileSize.toDouble()
                    x++
                    fileSize /= 1024.toDouble().toLong()
                }
                returnSize = changeSize.toString() + gubn[gubnKey]
            } catch (ex: Exception) {
                returnSize = "0.0 Byte"
            }

            println("getFileSize:$returnSize")
            return returnSize

        }

        fun classifyFormType(getFormInfo: LinkedTreeMap<String, Any>, getFormInfoTypeVo: GetFormInfoTypeVo, userInfo: UserInfoVO, gson: Gson): Bundle {
            var ozFormFileName = "" //오즈 이폼 서식 이름
            var ozFormJsonData = ""
            var targetName = ""
            var bundle = Bundle()

            try {
                if (getFormInfo.get("result") != null) {
                    /**
                     * 연명의료 계획서
                     */
                    if (getFormInfoTypeVo.formCode.equals("01")) {
                        ozFormFileName = "lstplan"
                        if (getFormInfoTypeVo.tempFormType.equals("N")) {
                            Dlog.e("lstplan 연명의료 계획서")
                            var lstplanDetailInfoVO: LstplanDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), LstplanDetailInfoVO::class.java)
                            ozFormJsonData = gson.toJson(lstplanDetailInfoVO)
                            targetName = lstplanDetailInfoVO.patiusernm.toString()
                        } else {
                            Dlog.e("lstplantemp 연명의료 계획서")
                            var lstplanTempDetailInfoVO: LstplanTempDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), LstplanTempDetailInfoVO::class.java)
                            ozFormJsonData = gson.toJson(lstplanTempDetailInfoVO)
                            targetName = lstplanTempDetailInfoVO.patiusernm.toString()
                        }
                        /**
                         * 사전연명의료 의향서
                         */
                    } else if (getFormInfoTypeVo.formCode.equals("02")) {
                        ozFormFileName = "addt"
                        if (getFormInfoTypeVo.tempFormType.equals("N")) {
                            Dlog.e("addt 사전연명의료 의향서")
                            var addtDetailInfoVO: AddtDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), AddtDetailInfoVO::class.java)
                            ozFormJsonData = gson.toJson(addtDetailInfoVO)
                            targetName = addtDetailInfoVO.wrtusernm.toString()
                        } else {
                            Dlog.e("addttemp 사전연명의료 의향서")
                            var addtTempDetailInfoVO: AddtTempDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), AddtTempDetailInfoVO::class.java)
                            ozFormJsonData = gson.toJson(addtTempDetailInfoVO)
                            targetName = addtTempDetailInfoVO.wrtusernm.toString()
                        }
                        /**
                         * 임종과정에 있는 환자 판단서
                         */
                    } else if (getFormInfoTypeVo.formCode.equals("03")) {
                        ozFormFileName = "edfjatwapid"
                        if (getFormInfoTypeVo.tempFormType.equals("N")) {
                            Dlog.e("edfjatwapid 임종과정에 있는 환자 판단서")
                            var basicEdfjatwapidInfoVO: BasicEdfjatwapidInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), BasicEdfjatwapidInfoVO::class.java)
                            ozFormJsonData = gson.toJson(basicEdfjatwapidInfoVO)
                            targetName = basicEdfjatwapidInfoVO.patiusernm.toString()
                        } else {
                            Dlog.e("edfjatwapidTemp 임종과정에 있는 환자 판단서")
                            var basicEdfjatwapidTempInfoVO: BasicEdfjatwapidTempInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), BasicEdfjatwapidTempInfoVO::class.java)
                            ozFormJsonData = gson.toJson(basicEdfjatwapidTempInfoVO)
                            targetName = basicEdfjatwapidTempInfoVO.patiusernm.toString()
                        }
                        /**
                         * 연명의료중단등 결정에 대한 환자의사 확인서( 사전연명의료의향서 )
                         */
                    } else if (getFormInfoTypeVo.formCode.equals("04")) {
                        ozFormFileName = "edfvopiaddt"
                        if (getFormInfoTypeVo.tempFormType.equals("N")) {
                            Dlog.e("edfvopiaddt 연명의료중단등 결정에 대한 환자의사 확인서( 사전연명의료의향서 )")
                            var basicEdfvopiaddtInfoVO: BasicEdfvopiaddtInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), BasicEdfvopiaddtInfoVO::class.java)
                            ozFormJsonData = gson.toJson(basicEdfvopiaddtInfoVO)
                            targetName = basicEdfvopiaddtInfoVO.patiusernm.toString()
                        } else {
                            Dlog.e("edfvopiaddttemp 연명의료중단등 결정에 대한 환자의사 확인서( 사전연명의료의향서 )")
                            var basicEdfvopiaddtTempInfoVO: BasicEdfvopiaddtTempInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), BasicEdfvopiaddtTempInfoVO::class.java)
                            ozFormJsonData = gson.toJson(basicEdfvopiaddtTempInfoVO)
                            targetName = basicEdfvopiaddtTempInfoVO.patiusernm.toString()
                        }
                        /**
                         * 연명의료중단등 결정에 대한 환자의사 확인서 ( 환자가족 진술 )
                         */
                    } else if (getFormInfoTypeVo.formCode.equals("05")) {
                        //서식요청 코드 (03) 환자 가족 서명
                        ozFormFileName = "edfvopisotpf"
                        if (getFormInfoTypeVo.requestCode.equals("03")) {
                            if (getFormInfoTypeVo.tempFormType.equals("N")) {
                                Dlog.e("edfvopisotpf 연명의료중단등 결정에 대한 환자의사 확인서 ( 환자가족서명)")
                                var edfvopisotpfSignInfoVO: EdfvopisotpfDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdfvopisotpfDetailInfoVO::class.java)
                                ozFormJsonData = gson.toJson(edfvopisotpfSignInfoVO)
                                targetName = edfvopisotpfSignInfoVO.patiusernm.toString()
                            } else {
                                Dlog.e("edfvopisotpftemp 연명의료중단등 결정에 대한 환자의사 확인서 ( 환자가족서명)")
                                var edfvopisotpfsignTempInfoVO: EdfvopisotpfTempDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdfvopisotpfTempDetailInfoVO::class.java)
                                ozFormJsonData = gson.toJson(edfvopisotpfsignTempInfoVO)
                                targetName = edfvopisotpfsignTempInfoVO.patiusernm.toString()
                            }
                            //서식요청 코드 (04)	 전문의 협진
                        } else if (getFormInfoTypeVo.requestCode.equals("04")) {
                            if (getFormInfoTypeVo.tempFormType.equals("N")) {
                                Dlog.e("edfvopisotpf 서식요청 코드 (04) 연명의료중단등 결정에 대한 환자의사 확인서 ( 전문의협진)")
                                var edfvopisotpfDetailInfoVO: EdfvopisotpfDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdfvopisotpfDetailInfoVO::class.java)
                                ozFormJsonData = gson.toJson(edfvopisotpfDetailInfoVO)
                                targetName = edfvopisotpfDetailInfoVO.patiusernm.toString()
                            } else {
                                Dlog.e("edfvopisotpftemp 서식요청 코드 (04) 연명의료중단등 결정에 대한 환자의사 확인서 ( 전문의협진)")
                                var edfvopisotpfTempDetailInfoVO: EdfvopisotpfTempDetailInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdfvopisotpfTempDetailInfoVO::class.java)
                                ozFormJsonData = gson.toJson(edfvopisotpfTempDetailInfoVO)
                                targetName = edfvopisotpfTempDetailInfoVO.patiusernm.toString()
                            }
                        } else {
                            // 잘못된 서식요청 코드
                            ozFormFileName = "잘못된 서식요청 코드"
                        }
                        /**
                         * 연명의료중단등 결정에 대한 친권자 및 환자가족 의사 확인서
                         */
                    } else if (getFormInfoTypeVo.formCode.equals("06")) {
                        // 서식요청코드 (03) 가족서명
                        ozFormFileName = "edffamconfr"
                        if (getFormInfoTypeVo.requestCode.equals("03")) {
                            if (getFormInfoTypeVo.tempFormType.equals("N")) {
                                Dlog.e("edfvopisotpftemp 서식요청 코드 (06) 연명의료중단등 결정에 대한 친권자 및 환자가족 의사 확인서 ( 가족서명)")
                                var edffamconfrDetailInfoVO: EdffamconfrDetailInfo = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdffamconfrDetailInfo::class.java)
                                ozFormJsonData = gson.toJson(edffamconfrDetailInfoVO)
                                targetName = edffamconfrDetailInfoVO.edffamconfrDetailInfo!!.patiusernm.toString()
                            } else {
                                Dlog.e("edfvopisotpftemp 서식요청 코드 (06) 연명의료중단등 결정에 대한 친권자 및 환자가족 의사 확인서 ( 가족서명)")
                                var edffamconfrTempDetailInfoVO: EdffamconfrTempDetailInfo = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdffamconfrTempDetailInfo::class.java)
                                ozFormJsonData = gson.toJson(edffamconfrTempDetailInfoVO)
                                targetName = edffamconfrTempDetailInfoVO.edffamconfrDetailInfo!!.patiusernm.toString()
                            }
                            // 서식요청코드 (04) 전문의 협진
                        } else if (getFormInfoTypeVo.requestCode.equals("04")) {
                            if (getFormInfoTypeVo.tempFormType.equals("N")) {
                                Dlog.e("edffamconfr 서식요청코드 (04) 연명의료중단등 결정에 대한 친권자 및 환자가족 의사 확인서 ( 전문의협진)")
                                var edffamconfrDetailInfoVO: EdffamconfrDetailInfo = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdffamconfrDetailInfo::class.java)
                                ozFormJsonData = gson.toJson(edffamconfrDetailInfoVO)
                                targetName = edffamconfrDetailInfoVO.edffamconfrDetailInfo!!.patiusernm.toString()
                            } else {
                                Dlog.e("edffamconfrTemp 서식요청코드 (04) 연명의료중단등 결정에 대한 친권자 및 환자가족 의사 확인서 ( 전문의협진)")
                                var edffamconfrTempDetailInfoVO: EdffamconfrTempDetailInfo = gson.fromJson(gson.toJson(getFormInfo.get("result")), EdffamconfrTempDetailInfo::class.java)
                                ozFormJsonData = gson.toJson(edffamconfrTempDetailInfoVO)
                                targetName = edffamconfrTempDetailInfoVO.edffamconfrDetailInfo!!.patiusernm.toString()
                            }
                        } else {
                            // 잘못된 서식요청 코드
                            ozFormFileName = "잘못된 서식요청 코드"
                        }
                        /**
                         * 잘못된 서식 요청
                         */
                    } else {
                        // 잘못된 서식요청 코드
                        ozFormFileName = "잘못된 서식요청"
                    }
                } else {
                    ozFormFileName = "결과값 없음"
                }
            }catch (e:java.lang.Exception){

                Dlog.e(e.stackTrace.toString())

                ozFormFileName = "데이터 파싱 에러"
                bundle.putString("ozFileName", ozFormFileName)
                bundle.putString("userInfo", gson.toJson(userInfo))
                bundle.putString("jsonData",ozFormJsonData)
                bundle.putString("targetName",targetName)
                return bundle
            }

            bundle.putString("ozFileName", ozFormFileName)
            bundle.putString("userInfo", gson.toJson(userInfo))
            bundle.putString("jsonData",ozFormJsonData)
            bundle.putString("targetName",targetName)
            return bundle
        }

        fun getCalculatePeriod(month: Int):String{
            val tz = TimeZone.getTimeZone("Asia/Seoul")
            var calendar = Calendar.getInstance(tz)
            calendar.add(Calendar.MONTH, month)
            var sdf:SimpleDateFormat = SimpleDateFormat("yyyyMMdd", Locale.KOREA)
            Dlog.e("sdf.format(calendar.time) : " + sdf.format(calendar.time))
            return sdf.format(calendar.time)
        }

        fun divideDialogTitle(code: String) = when(code){
            "01"->"작성자 서명하기"
            "02"->"환자 서명하기"
            "03"->"환자 가족 서명하기"
            "04"->"전문의 서명하기"
            "05"->"법정대리인 서명하기"
            "06"->"작성자 확인 서명 요청"
            else-> "구분못함"
        }

        fun getToDateforDialog():String {
            var date = Date()
            var sdf:SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.KOREA)
            return sdf.format(date)
        }

        fun divideDialogTargetName(code: String, name: String) = when(code){
            "01"-> "환자명 : $name"
            "02"-> "작성자명 : $name"
            "03"-> "환자명 : $name"
            "04"-> "환자명 : $name"
            "05"-> "환자명 : $name"
            "06"-> "환자명 : $name"
            else-> "구분못함"
        }

        fun showToast(message: String){
            Toast.makeText(BaseApplication.globalApplicationContext,message,Toast.LENGTH_LONG).show()
        }

        fun checkLoginType(certificationCode: String): String{
            var loginType = ""
            when (certificationCode.substring(0,1)){
                "C"->{
                    Dlog.e("C = 웹사이트인증")
                    loginType = "03"
                }
                "S"->{
                    Dlog.e("S = 서식서명인증")
                    loginType = "04"
                }
                "J"->{
                    Dlog.e("J = 협진인증")
                    loginType = "05"
                }
                "A"->{
                    Dlog.e("A = 등록자서명인증")
                    loginType = "08"
                }
            }
            return loginType
        }

//        fun convertGson(jsonString: String){
//            val type = object:TypeToken<Map<String, Any>>() {}.getType()
//            val data = Gson().fromJson(jsonString, type)
//            val it = data.entrySet().iterator()
//            while (it.hasNext())
//            {
//                val entry = it.next()
//                if (entry.getValue() == null)
//                {
//                    it.remove()
//                }
//                else if (entry.getValue().getClass().equals(ArrayList::class.java))
//                {
//                    if ((entry.getValue() as ArrayList<*>).size() === 0)
//                    {
//                        it.remove()
//                    }
//                }
//            }
//        }
    }

}
