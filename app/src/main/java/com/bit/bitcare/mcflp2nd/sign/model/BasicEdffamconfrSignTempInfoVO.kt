package com.bit.bitcare.mcflp2nd.sign.model


data class BasicEdffamconfrSignTempInfoVO(
        // @ApiModelProperty(value = "친권자 및 환자가족 의사 확인서 서명  임시 일련번호", example = "2")
        var edffamconfrsigntempsn: Int?,            //bigint NOT NULL,
// @ApiModelProperty(value = "친권자 및 환자가족 의사 확인서 임시 일련번호", example = "3")
        var edffamconfrtempsn: Int?,            //bigint NOT NULL,
// @ApiModelProperty(value = "가족관계코드(10:배우자, 20:직계비속, 30:직계존속, 40:형제자매)", example = "10")
        var famrelcode: String?,              //character varying(2) COLLATE pg_catalog."default" NOT NULL,
        var famrelcodenm: String?,
// @ApiModelProperty(value = "환자가족성명", example = "가족1")
        var famnm: String?,                   //character varying(50) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자가족생년월일", example = "1988-01-01")
        var fambday: String?,                 //date NOT NULL,
// @ApiModelProperty(value = "환자가족연락처", example = "010-111-2222")
        var famtn: String?,                   //character varying(14) COLLATE pg_catalog."default" NOT NULL,
// @ApiModelProperty(value = "환자가서명이미지", example = "data:image/png: String?,base64,iVBORw0KGgoAAAANSUhEUgAAAcIAAAGQCAYAAAA9XmC5AAAABHNCSVQICAgIfAhkiAAAFEhJREFUeJzt3d11GlnagNHTs+a+5AhQR4A6AuQIhCMARQCKQCgC4QhcisA4AtERCEcAEwFMBMxFf/hzqy2oKkBU8e69ltZc2BZluYeHU+enfluv1+sEAEH969QXAACnJIQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhACEJoQAhCaEAIQmhMBOf/75Z3p4eEgfP35Mv/32W/rw4UO6vb1Nq9Xq1JcGe/ttvV6vT30RQL38+eefaTqd/vh6y/39fRqNRu92XXAMQgiklFL69u1bmkwmaTKZFB7pXVxcpOVyeeQrg+MSQgisSvxe8xZC0/371BcAvK/ZbJaenp5Snud7z/F1Op0DXRWcjhBCAKvVKn379i2Nx+M0m832/n5ZlqVut5vG4/EBrg5OSwjhjC0Wi/Tw8JDyPN/r+2RZlq6vr398XV1dHeYCoQaEEM7QarVKd3d3ewXw5uZG+AhBCOGMrFar9Pnz5zQejyvN//V6vXR9fZ263W66uLg4whVC/QghnIF9Anhzc5O63a74EZYQQsM9PT2l0WiUFotF4T/TarXScDhM3W43XV5eHu3aoAmEEBpqOp2m29vbUgHs9Xqp3++n6+vro10XNI0QQsPMZrN0d3e39eiz13q9XhqNRkZ/8AtCCA1RZSWoAMJuQggN8PDwUGohTKfTSePx2LYHKEAIocYmk0m6u7srPA/YbrfTeDw2BwglCCHU0GKxSLe3t4XnAVutVhqNRqnf7x/1uuAceTAv1MhmHvD3338vFMEsy9L9/X1aLBYiCBUZEUJNPD09peFwWHgesNfrpfF4bBM87EkI4cTKboewEAYOy61ROJHNbdA//vijUARbrVb6+vVrmk6nIggHZEQIJzCZTNLt7W2h26BZlqXhcJhGo9HxLwwCEkJ4R6vVKt3e3qbJZFLo95sHhONzaxTeydPTU/r9998LRbDdbqfn5+eU57kIwpEZEcKRldkTmGVZGo1GaTgcHv/CgJSSESEc1efPnwsvhhkMBmmxWIggvDMjQjiC2WyWbm9v02w22/l7W61WyvPcsWhwIkaEcGAPDw/pjz/+KBTB+/v7NJvNRBBOyIgQDmQ6naa7u7tCAWy32ynPc/sBoQaMCGFPm43xHz9+3BnBzdmgs9lMBKEmjAhhD9PpNN3e3hZ6TFKn00l5nh/8IbmLxSL95z//+bEg5/X/ppTS9fV16vf7qdfrHfS14Rz8tl6v16e+CGiaMhvjj7ElYjqdpqenpzSZTAof0p1SSo+Pj1alwitCCCWVOR7t5uYmjcfjg4wCF4tFenp6SnmeF35Q72tXV1fp5eVl72sparVapW/fvqXpdJpms1mazWbp8vIyXV1dpcfHx4OPjqEKIYSCFotFuru7KzwKzPM8dbvdvV5zE5LxeFxoEU4Rx/6/fNFrvri4SC8vL2LIyVksAwVsNsYXiWCv10uLxaJyBFerVXp6ekqfPn1KHz58SP1+/2AR7HQ6B/k+v7I5QafoNa9WqzQej492PVCUxTKwRZnj0fbZGL8ZRU0mk8IHcldxjPnBxWKRHh4eUp7npf/soQIP+xBCeMPDw0PhRx8NBoM0Go1KH5C9iUjZRS+vdTqddHl5mWazWfr+/fsvf8/Nzc3et2p/tk8ANxwoTi2sgb95eXlZX11drVNKO7/a7fb6+fm59GvM5/N1v98v9BrbXvvLly/r5XL547ovLi7e/P3z+fwgP59DXPvm68uXLwe5JtiHEML/WS6X69FoVPhN/P7+vvRrPD8/7xWRVqu1HgwG/4jacrncGsEq1/raIQO4+btAHQghrP8K1OXlZaE38E6ns355eSn9/a+vrysFI8uyda/Xe3PkuVwut45g2+32Xj+bqgHs9Xrrdrv95q9XGUnDMQghoS2Xy/VwOCwcpMfHx1LfP8/zwrdZfxW/r1+/7rz+bd8/y7LKt0T3CeB8Pl/P5/OtvwfqQggJq+wosGhQlsvlejweF/7erwOxK34/v86uyFYZdS2Xy70CuLFtBHyo+Uo4BCEkpKJzgVmWFV7QMZ/P18PhcOtc3bbRX5k4FIlg2YUomznSstf/q2v/+vWr0SCNIYSEUmZF6M3NzY8VmdtUXQCTZdn6/v6+0Gu8tuv1ysYmz/PSI9i34r1cLt/8XvvcqoVjEULCGI/Hhd7gW61WoduT+8z/VQ3gen3YCFZZxLNr9Lrt+g6xerWuXs+p9vv9U18SBQkhZ28+nxd+sx8MBlsDtbl9WGX+r9Vq7b1vbjAYHCSC8/l83e12DxrA9Xr7LdFz3S6x7ZbyYDA49eVRgBBy1r58+VJozivLsq2jwH32/3U6nYNsHP/y5cveESy7V7JoANfrv+K67Wd9jtslxuPx1r/zxcXFqS+RAoSQs7RcLguPeN6aC9xn9ecmIId6898VwSJ7BfM8L7UQpsxK2fV6+yrRcxsZlZlTpf78")
        var signimgfilegrpno: String?,        //bigint NOT NULL,
// @ApiModelProperty(value = "등록프로그램", example = "PG001")
        var enterpgm: String?,            // character varying(200) NOT NULL, -- 등록프로그램
// @ApiModelProperty(value = "등록일시", example = "2017-12-04")
        var enterdate: String?,            // timestamp without time zone NOT NULL, -- 등록일시
// @ApiModelProperty(value = "등록자아이디", example = "3333")
        var enterid: String?,                // character varying(50) NOT NULL, -- 등록자아이디
// @ApiModelProperty(value = "등록자명", example = "emdfhrwkaud")
        var enternm: String?,                // character varying(50) NOT NULL, -- 등록자명
// @ApiModelProperty(value = "수정프로그램", example = "PG001")
        var updatepgm: String?,            // character varying(200), -- 수정프로그램
// @ApiModelProperty(value = "수정일시", example = "2017-12-04")
        var updatedate: String?,            // timestamp without time zone, -- 수정일시
// @ApiModelProperty(value = "수정자아이디", example = "4444")
        var updateid: String?,            // character varying(50), -- 수정자아이디
// @ApiModelProperty(value = "수정자명", example = "수정자명")
        var updatenm: String?            // character varying(50), -- 수정자명
)
