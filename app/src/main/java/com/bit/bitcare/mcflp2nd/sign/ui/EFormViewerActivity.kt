package com.bit.bitcare.mcflp2nd.sign.ui

import android.content.Intent
import android.content.Intent.*
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.androidnetworking.interfaces.UploadProgressListener
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.*
import com.bit.bitcare.mcflp2nd.sign.ui.signature.DateCheckException
import com.bit.bitcare.mcflp2nd.sign.ui.signature.NoSignatureException
import com.bit.bitcare.mcflp2nd.sign.ui.signature.SignatureDialogBuilder
import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util
import com.google.gson.Gson
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_e_form_viewer.*
import kotlinx.android.synthetic.main.toolbar_sign.*
import org.json.JSONObject
import oz.api.OZReportAPI
import oz.api.OZReportCommandListener
import oz.api.OZReportViewer
import oz.viewer.ui.dlg.OZErrorMsgUI
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

class EFormViewerActivity : BaseActivity() {
    companion object {
        init {
            System.loadLibrary("skia_android")
            System.loadLibrary("ozrv")
        }
    }

    private var viewer: OZReportViewer? = null
    private var signatureDialogBuilder: SignatureDialogBuilder? = null
    private var ozFileName = ""
    private var userInfo = ""
    private var jsonData = ""
    private var formInfo = ""
    private var title = ""
    private var targetName = ""
    private var patientName = ""
    private var msafjdgmtcont = ""
    private var msafjdgmtday = ""
    private var regday = ""
    private var userInfoVo: UserInfoVO? = null
    private var formInfoVo: GetFormInfoTypeVo? = null
    private var isRegistedUsersRequest = false //비회원 서명완료 후 사용

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_e_form_viewer)

        try {
            ozFileName = intent?.extras?.getString("ozFileName")!!
            userInfo = intent?.extras?.getString("userInfo")!!
            jsonData = intent?.extras?.getString("jsonData")!!
            formInfo = intent?.extras?.getString("formInfo")!!
            targetName = intent?.extras?.getString("targetName")!!

        } catch (e: Exception) {
            MaterialDialog(this)
                    .title(R.string.error)
                    .message(R.string.error_message)
                    .positiveButton(R.string.back) {
                        onBackPressed()
                    }
                    .show()
        }

        Dlog.e("받아온 ozFileName : $ozFileName")
        Dlog.e("받아온 userInfo : $userInfo")
        Dlog.e("받아온 data : $jsonData")
        Dlog.e("받아온 formInfo : $formInfo")

        toolbar_title.text = targetName  //타이틀 세팅

        userInfoVo = gson.fromJson(userInfo, UserInfoVO::class.java)
        formInfoVo = gson.fromJson(formInfo, GetFormInfoTypeVo::class.java)
        title = Util.divideDialogTitle(formInfoVo!!.requestCode!!)
        patientName = Util.divideDialogTargetName(formInfoVo!!.formCode!!, targetName)

        iv_toorbar_back_button.setOnClickListener {
            onBackPressed()
        }

        if ("Y".equals(formInfoVo!!.cancelType)) { //철회요청인가?
            toolbar_sign_button.setText("철회서명")
            title = "철회 서명하기"
        }

        if (!"01".equals(formInfoVo!!.requestState)) { //서명대기 상태가 아니면
            toolbar_sign_button.visibility = View.GONE
        }

        if ("03".equals(formInfoVo!!.formCode)) { //판단서 일때
            val basicEdfjatwapidTempInfoVO: BasicEdfjatwapidTempInfoVO = makePatientJudgementVo(jsonData)
            msafjdgmtcont = basicEdfjatwapidTempInfoVO.msafjdgmtcont!!
            msafjdgmtday = basicEdfjatwapidTempInfoVO.msafjdgmtday!!
            regday = basicEdfjatwapidTempInfoVO.regday!!
        }

        signatureDialogBuilder = SignatureDialogBuilder()

        toolbar_sign_button.setOnClickListener {
            SignatureDialogBuilder()
                    .show(this, object : SignatureDialogBuilder.SignatureEventListener {
                        override fun onSignatureEntered(savedFile: File) {
                            addSignImg(savedFile, "", "")
                        }

                        override fun onSignatureEntered(savedFile: File, date: String, comment: String) {
                            addSignImg(savedFile, date, comment)
                        }

                        override fun onSignatureInputCanceled() {
                            Toast.makeText(this@EFormViewerActivity, "서명하기 취소", Toast.LENGTH_SHORT).show()
                        }

                        override fun onSignatureInputError(e: Throwable) {
                            when (e) {
                                is NoSignatureException -> Toast.makeText(this@EFormViewerActivity, "서명이 없습니다.", Toast.LENGTH_SHORT).show()
                                // They clicked confirm without entering anything
                                //                                doSomethingOnNoSignatureEntered()
                                is DateCheckException -> Toast.makeText(this@EFormViewerActivity, "올바른 날짜를 선택해주세요.", Toast.LENGTH_SHORT).show()
                                else -> Toast.makeText(this@EFormViewerActivity, "서명 에러", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onSignatureInputReset() {

                        }
                    }, title, formInfoVo!!.formCode, formInfoVo!!.requestCode, patientName, msafjdgmtcont, msafjdgmtday, regday)
        }

        var result = checkPermissions(Define.needFilePermissions, Define.REQUEST_FILE_PERMISSION)
        Dlog.e("checkPermissions result : $result")
        if (result) {
            initOZViewer()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        signatureDialogBuilder?.dismiss()
    }

    override fun onBackPressed() {
        if ("".equals(PreferenceUtil(this@EFormViewerActivity).getStringExtra("userSN"))) {
            //userSN 없으면 나간 후 다시 서명코드 쓰면 들어올 수 있다고 다이어로그 띄워줌
            if(isRegistedUsersRequest){
                isRegistedUsersRequest = false
                toolbar_sign_button.text = "서명완료"
                toolbar_sign_button.isFocusable = false
                toolbar_sign_button.isEnabled = false
                getFormInfo()
            }else{
                MaterialDialog(this)
                        .title(R.string.go_out)
                        .message(R.string.go_out_message)
                        .positiveButton(R.string.go_out) {
                            finish()
                        }
                        .negativeButton(R.string.cancle) {

                        }
                        .show()
            }
        } else {
            finish()
        }
    }

    private fun initOZViewer() {
        Dlog.e("OZViewer started")

        val oZReportCommandListener = object : OZReportCommandListener() {
            override fun OZUserEvent(param1: String, param2: String, param3: String): String {
                return ""
            }

            override fun OZErrorCommand(code: String?, message: String?, detailmessage: String?, reportname: String?) {
                Dlog.e("code : $code")
                Dlog.e("message : $message")
                Dlog.e("detailmessage : $detailmessage")
                Dlog.e("reportname : $reportname")
//                super.OZErrorCommand(code, message, detailmessage, reportname)

                MaterialDialog(this@EFormViewerActivity)
                        .title(R.string.error)
                        .message(null, message)
                        .positiveButton(R.string.close) {
                            Dlog.e("종료 - 예")
                            finish()
                        }
                        .show()
            }
        }

        var userInfoData: String? = setUserInfo()
        //오즈 뷰어 패러미터 설정

        Dlog.e("seok Define.ozEndPoint: " + Define.ozEndPoint)

        val servlet = "connection.servlet=" + Define.ozEndPoint
        val reportname = "\n" + "connection.reportname=/web/" + ozFileName + "_v1.ozr"
        var pcount = "\n" + "connection.pcount=2"
        val args1 = "\n" + "connection.args1=jsondata=" + jsonData
        val args2 = "\n" + "connection.args2=userInfo=" + userInfoData
        val errorcommand = "\n" + "viewer.errorcommand=true"
        val font = "\n" + "font.fontnames=font1,font2"
        val font1name = "\n" + "font.font1.name=돋움"
        val font1url = "\n" + "font.font1.url=res://dotum.ttc"
        val font2name = "\n" + "font.font2.name=돋움체"
        val font2url = "\n" + "font.font2.url=res://dotum.ttc"
        val smartframesize = "\n" + "viewer.smartframesize=true"
        val zoombydoubletap = "\n" + "viewer.zoombydoubletap=false"
        val minzoom = "\n" + "viewer.minzoom=40"
        val usetoolbar = "\n" + "viewer.usetoolbar=true"
        val progresscommand = "\n" + "viewer.progresscommand=true"
        val signpad_type = "\n" + "eform.signpad_type=zoom"
        var params = servlet + reportname + pcount + args1 + args2 + errorcommand + font + font1name + font1url + font2name + font2url

//        var params = servlet + reportname + pcount + args1 + args2 + zoombydoubletap + minzoom + usetoolbar + progresscommand + signpad_type + smartframesize
//        var params = servlet + reportname + pcount + args1 + args2 + zoombydoubletap + minzoom + usetoolbar + progresscommand + signpad_type
//        var params = servlet + reportname + pcount + args1 + args2 + zoombydoubletap + minzoom + usetoolbar + progresscommand + signpad_type

        //오즈 뷰어를 표시할 View 또는 ViewGroup 생성
        val frameLayout = FrameLayout(this)

        //오즈 뷰어 생성
        try {
            viewer = OZReportAPI.createViewer(frameLayout, oZReportCommandListener, params)
            e_form_viewer.addView(frameLayout)
        } catch (e: FileNotFoundException) {
            //권한이 없어서 파일을 못찾을 경우인데
            //권한이 없어도 뷰어가 잘뜸 하지만 메모리 릭 발생 그래서 권한 필수로 '허용' 받음
        }
    }

    private fun setUserInfo(): String? {
        var sdf: SimpleDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.KOREA)
        var d: Date = Date()
        var strCT: String = sdf.format(d)

        var UserInfoVO: UserInfoVO = gson.fromJson(userInfo, UserInfoVO::class.java)
        return UserInfoVO.orgregnm + " " + UserInfoVO.usernm + "(" + UserInfoVO.usersn + ") " + strCT
    }

    private fun addSignImg(file: File?, date: String, comment: String) {
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("addSignImg token : " + token)

        var signFormSN = PreferenceUtil(this@EFormViewerActivity).getStringExtra("signFormSN")
        Dlog.e("signFormSN  : " + signFormSN)

        Dlog.e("date  : " + date)
        Dlog.e("comment  : " + comment)

        val uploadProgressListener = object : UploadProgressListener {
            override fun onProgress(bytesUploaded: Long, totalBytes: Long) {
                Log.e("send file", bytesUploaded.toString() + "bytes")
                var per: Long = bytesUploaded * 100 / totalBytes
                Log.e("send file per", per.toString() + "%")
            }
        }

        if ("Y" == formInfoVo!!.cancelType) { //철회요청인가
            AndroidNetworking.upload(Define.serverIP + "/sign/addCancelSignImg")
                    .addMultipartParameter("token", token)
                    .addMultipartParameter("signFormSN", signFormSN)
                    .addMultipartFile("singImg", file)
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener(uploadProgressListener)
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            Dlog.e("addSignImg 받아옴??")
                            Dlog.e(response.toString())

                            var addSignImgVo = gson.fromJson(response.toString(), AddSignImgVo::class.java)
                            Dlog.e(addSignImgVo.toString())

                            if("100".equals(addSignImgVo.serviceCode)){
                                var addSignImgVo = gson.fromJson(response.toString(), AddSignImgVo::class.java)
                                Dlog.e(addSignImgVo.toString())

                                PreferenceUtil(this@EFormViewerActivity).putStringExtra("token", addSignImgVo.token.toString().replace("\"", ""))
                                Dlog.e("getFormListVo.token.toString() : " + addSignImgVo.token.toString())

                                Toast.makeText(this@EFormViewerActivity, "철회요청 서명이 완료되었습니다.", Toast.LENGTH_LONG).show()

                                if (addSignImgVo.result.fileName != null || addSignImgVo.result.fileURL != null) {
                                    onBackPressed()
                                }
                            }else{
                                Toast.makeText(this@EFormViewerActivity, addSignImgVo.serviceMsg, Toast.LENGTH_LONG).show()
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            Dlog.e("addSignImg 에러??")
                            Toast.makeText(this@EFormViewerActivity, "이미지 업로드 에러.", Toast.LENGTH_LONG).show()
                        }
                    })
        } else {
            AndroidNetworking.upload(Define.serverIP + "/sign/addSignImg")
                    .addMultipartParameter("token", token)
                    .addMultipartParameter("signFormSN", signFormSN)
                    .addMultipartParameter("signDate", date)
                    .addMultipartParameter("signText", comment)
                    .addMultipartParameter("userName", targetName)
                    .addMultipartFile("singImg", file)
                    .setPriority(Priority.HIGH)
                    .build()
                    .setUploadProgressListener(uploadProgressListener)
                    .getAsJSONObject(object : JSONObjectRequestListener {
                        override fun onResponse(response: JSONObject) {
                            Dlog.e("addSignImg 받아옴??")
                            Dlog.e(response.toString())

                            var addSignImgVo = gson.fromJson(response.toString(), AddSignImgVo::class.java)
                            Dlog.e(addSignImgVo.toString())

                            if("100".equals(addSignImgVo.serviceCode)){
                                PreferenceUtil(this@EFormViewerActivity).putStringExtra("token", addSignImgVo.token.toString().replace("\"", ""))
                                Dlog.e("getFormListVo.token.toString() : " + addSignImgVo.token.toString())

                                if (addSignImgVo.result.fileName != null || addSignImgVo.result.fileURL != null) {
                                    Toast.makeText(this@EFormViewerActivity, "서명이 완료되었습니다.", Toast.LENGTH_LONG).show()
                                    if ("".equals(PreferenceUtil(this@EFormViewerActivity).getStringExtra("userSN"))) {
                                        isRegistedUsersRequest = true
                                    }
                                    onBackPressed()
                                }
                            }else{
                                Toast.makeText(this@EFormViewerActivity, addSignImgVo.serviceMsg, Toast.LENGTH_LONG).show()
                            }
                        }

                        override fun onError(error: ANError) {
                            // handle error
                            Dlog.e("addSignImg 에러??")
                            Toast.makeText(this@EFormViewerActivity, "이미지 업로드 에러.", Toast.LENGTH_LONG).show()
                        }
                    })
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Dlog.e("requestCode : $requestCode")
        Dlog.e("permissions : " + permissions[0])
        Dlog.e("grantResults : " + grantResults[0])

        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initOZViewer()
        } else {
            finish()
            Toast.makeText(this@EFormViewerActivity, "'허용' 해주셔야 이용 가능합니다.", Toast.LENGTH_LONG).show()
        }
    }


    private fun makePatientJudgementVo(jsonString: String): BasicEdfjatwapidTempInfoVO {
        var basicEdfjatwapidTempInfoVO: BasicEdfjatwapidTempInfoVO = gson.fromJson(jsonString, BasicEdfjatwapidTempInfoVO::class.java)
        Dlog.e("edfjatwapid 임종과정에 있는 환자 판단서")
        return basicEdfjatwapidTempInfoVO
    }


    // 비회원 전문의 일때만 사용
    private fun getFormInfo(){
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("getFormInfo token : " + token)

        AndroidNetworking.post(Define.serverIP+"/sign/getFormInfo")
                .addBodyParameter("token", token)
                .addBodyParameter("signFormSN", formInfoVo!!.signFormSN)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("getFormInfo 받아옴??")
                        Dlog.e(response.toString())

                        var getFormInfo: LinkedTreeMap<String, Any> = gson.fromJson(response.toString(), object : TypeToken<LinkedTreeMap<String, Any>>() {}.type)
                        Dlog.e(getFormInfo.get("serviceCode").toString())

                        if("100.0".equals(getFormInfo.get("serviceCode").toString()) || "100".equals(getFormInfo.get("serviceCode").toString())) {
                            Dlog.e("getFormInfo.get(\"token\") : "+getFormInfo.get("token") as String)
                            PreferenceUtil(this@EFormViewerActivity).putStringExtra("token", getFormInfo.get("token") as String)

                            var getFormInfoTypeVo: GetFormInfoTypeVo = gson.fromJson(gson.toJson(getFormInfo.get("formType")), GetFormInfoTypeVo::class.java)
                            Dlog.e("getFormInfoTypeVo : " + getFormInfoTypeVo.toString())

                            PreferenceUtil(this@EFormViewerActivity).putStringExtra("signFormSN", getFormInfoTypeVo.signFormSN)
                            Dlog.e("getFormInfoTypeVo.signFormSN : " + getFormInfoTypeVo.signFormSN)

                            var userInfo: UserInfoVO = gson.fromJson( gson.toJson(getFormInfo.get("userInfo")), UserInfoVO::class.java)
                            Dlog.e("userInfo : " + userInfo.toString())

                            var bundle = Util.classifyFormType(getFormInfo, getFormInfoTypeVo, userInfo, gson)

                            if(bundle.getString("ozFileName").equals("잘못된 서식요청 코드") || bundle.getString("ozFileName").equals("잘못된 서식요청") || bundle.getString("ozFileName").equals("결과값 없음") || bundle.getString("ozFileName").equals("데이터 파싱 에러")){
                                Toast.makeText(this@EFormViewerActivity, bundle.getString("ozFileName"), Toast.LENGTH_LONG).show()
                            }else{
                                this@EFormViewerActivity.jsonData = bundle.getString("jsonData")!!
//                            this@EFormViewerActivity.userInfo = bundle.getString("userInfo")!!
//                            this@EFormViewerActivity.formInfo = gson.toJson(getFormInfoTypeVo)
                                userInfoVo = userInfo
                                formInfoVo = getFormInfoTypeVo
                                title = Util.divideDialogTitle(formInfoVo!!.requestCode!!)
                                patientName = Util.divideDialogTargetName(formInfoVo!!.formCode!!, targetName)
                                e_form_viewer.removeAllViews()
                                var result = checkPermissions(Define.needFilePermissions, Define.REQUEST_FILE_PERMISSION)
                                Dlog.e("checkPermissions result : " + result)
                                if (result) {
                                    initOZViewer()
                                }
                            }
                        }else{
                            Toast.makeText(this@EFormViewerActivity, getFormInfo.get("serviceMsg") as String,Toast.LENGTH_LONG).show()
                        }

                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("getFormInfo 에러??")
                        Toast.makeText(this@EFormViewerActivity,"올바른 번호를 입력해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

}
