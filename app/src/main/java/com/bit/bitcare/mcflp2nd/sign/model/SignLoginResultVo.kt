package com.bit.bitcare.mcflp2nd.sign.model

data class SignLoginResultVo(
        var loginSN: String,
        var userSN: String,
        var signFormSN: String,
        var deviceSID : String,
        var appDeviceSN: String,
        var deviceRegID: String,
        var varifier: String,
        var userName: String,
        var orgName: String
)