package com.bit.bitcare.mcflp2nd.sign.service;

import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bit.bitcare.mcflp2nd.data.model.ResponseVo;
import com.bit.bitcare.mcflp2nd.sign.ui.CertificationInformationActivity;
import com.bit.bitcare.mcflp2nd.sign.utils.Define;
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import org.json.JSONObject;

/**
 * Created by acid on 2016-11-21.
 * 토큰 갱신용 클래스
 */

public class InstanceIDListenerService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        // 이 token을 서버에 전달 한다.

        Log.e("ID", "Refreshed token: " + refreshedToken);
        Log.e("ID", "Refreshed token: " + refreshedToken);
        Log.e("ID", "Refreshed token: " + refreshedToken);

        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        // 서버에 등록(DB 연동 관련 작업)

//        AndroidNetworking.post(Define.serverIP + "/sign/updateDeviceInfo")
//                .addBodyParameter("token", PreferenceUtil(getApplicationContext()).getStringExtra("token"))
//                .addBodyParameter("userSN", PreferenceUtil(getApplicationContext()).getStringExtra("userSN"))
//                .setPriority(Priority.HIGH)
//                .build()
//                .getAsJSONObject(object :JSONObjectRequestListener {
//            override fun onResponse(response:JSONObject) {
//                Dlog.e("받아옴??")
//                Dlog.e(response.toString())
//                var checkLoginKeyVo = gson.fromJson(response.toString(), ResponseVo::class.java)
//                Dlog.e(checkLoginKeyVo.toString())
//                Dlog.e(checkLoginKeyVo.result.toString())
//
//                PreferenceUtil(this@SplashActivity).putStringExtra("checkLoginKeyResultVo", gson.toJson(checkLoginKeyVo.result))
//
//                val intent = Intent(this@SplashActivity, CertificationInformationActivity::class.java)
//                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
//                startActivity(intent)
//                overridePendingTransition(0, 0)
//            }
//
//            override fun onError(error:ANError) {
//                // handle error
//                Dlog.e("자동 로그인 실패")
//                Toast.makeText(getBaseContext(), "자동 로그인 실패", Toast.LENGTH_LONG).show()
//            }
//        })

    }
}
