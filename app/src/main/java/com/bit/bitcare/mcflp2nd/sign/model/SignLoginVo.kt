package com.bit.bitcare.mcflp2nd.sign.model

data class SignLoginVo(
        var serviceCode: String,
        var serviceMsg: String,
        var token: String,
        var result: SignLoginResultVo
)