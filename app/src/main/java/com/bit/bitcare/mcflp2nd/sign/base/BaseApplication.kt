package com.bit.bitcare.mcflp2nd.sign.base

import android.content.Context
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.support.multidex.MultiDex
import android.support.multidex.MultiDexApplication
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.jacksonandroidnetworking.JacksonParserFactory
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric
import okhttp3.OkHttpClient
import java.util.*
import java.util.concurrent.ConcurrentHashMap

/**
 * Created by acid on 2018-08-02.
 */

class BaseApplication : MultiDexApplication() {
    init {
        instance = this
    }

    companion object {
        var instance: BaseApplication? = null
        val globalApplicationContext: BaseApplication?
            get() {
                if (instance == null)
                    throw IllegalStateException("this application does not inherit BaseApplication")
                return instance
            }
        var DEBUG: Boolean = true

        fun context(): Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()

        MultiDex.install(this)
        BaseApplication.DEBUG = isDebuggable(this)

        Fabric.with(this, Crashlytics())

        val scaleFactor = Resources.getSystem().displayMetrics.density
        val widthDp = Resources.getSystem().displayMetrics.widthPixels / scaleFactor
        val heightDp = Resources.getSystem().displayMetrics.heightPixels / scaleFactor

        val smallestWidth = Math.min(widthDp, heightDp)

        when {
            smallestWidth > 720 -> Dlog.e("Device is a 10\" inch smallestWidth $smallestWidth")
            smallestWidth > 600 -> Dlog.e("Device is a 7\" inch smallestWidth $smallestWidth")
            else -> Dlog.e("Device is a other smallestWidth $smallestWidth")
        }

        Dlog.e("density : $scaleFactor")
        Dlog.e("densityDpi : "+Resources.getSystem().displayMetrics.densityDpi)
        Dlog.e("heightPixels : "+Resources.getSystem().displayMetrics.heightPixels)
        Dlog.e("widthPixels : "+Resources.getSystem().displayMetrics.widthPixels)
        Dlog.e("scaledDensity : "+Resources.getSystem().displayMetrics.scaledDensity)

        val widthDpi = Resources.getSystem().displayMetrics.xdpi
        val heightDpi = Resources.getSystem().displayMetrics.ydpi

        val widthInches = Resources.getSystem().displayMetrics.heightPixels / widthDpi
        val heightInches = Resources.getSystem().displayMetrics.heightPixels / heightDpi

        val diagonalInches = Math.sqrt((widthInches * widthInches + heightInches * heightInches).toDouble())

        if (diagonalInches >= 10) {
            Dlog.e("Device is a 10\" inch Real $diagonalInches inch")
        }
        else if (diagonalInches >= 7) {
            Dlog.e("Device is a 7\" inch Real $diagonalInches inch")
        }
        else {
            Dlog.e("Device is a other $diagonalInches inch")
        }
        //Firebase initialization for custom setting
        val builder: FirebaseOptions.Builder = FirebaseOptions.Builder()
                .setApplicationId("1:338993658065:android:ef7a9c8d00b06bf1")
                .setApiKey("AIzaSyD7VbtvFamT685L5vzYx_spsdx_B86-jW4")
//                .setDatabaseUrl("https://exampleformcflp2nd.firebaseio.com")
//                .setStorageBucket("exampleformcflp2nd.appspot.com")
//
//        mcflp2ndFirebaseApp = FirebaseApp.initializeApp(this, builder.build(),"mcflp2ndFirebaseApp")
        FirebaseApp.initializeApp(this, builder.build())


        // Adding an Network Interceptor for Debugging purpose :
        val okHttpClient = OkHttpClient().newBuilder()
                .retryOnConnectionFailure(true)
                .build()
        AndroidNetworking.initialize(applicationContext, okHttpClient)

//        AndroidNetworking.initialize(applicationContext)
        AndroidNetworking.setParserFactory(JacksonParserFactory())
//        AndroidNetworking.enableLogging()
//        AndroidNetworking.enableLogging(HttpLoggingInterceptor.Level.BODY)

//        //TypeKit 적용
//        Typekit.getInstance()
//                .addNormal(Typekit.createFromAsset(this, "fonts/NanumBarunGothic.otf"))
//                .addBold(Typekit.createFromAsset(this, "fonts/NanumBarunGothicBold.otf"))
    }

    /**
     * 현재 디버그모드 여부를 리턴
     */
    private fun isDebuggable(context:Context):Boolean {
        var debuggable = false
        val pm = context.getPackageManager()
        try {
            val appinfo = pm.getApplicationInfo(context.getPackageName(), 0)
            debuggable = (0 != (appinfo.flags and ApplicationInfo.FLAG_DEBUGGABLE))
        }
        catch (e: PackageManager.NameNotFoundException) {
            /* debuggable variable will remain false */
        }
        return debuggable
    }

    private val mDataHolder = ConcurrentHashMap<String, Any>()

    fun putDataHolder(data: Any): String {
        //중복되지 않는 홀더 아이디를 생성해서 요청자에게 돌려준다.
        val dataHolderId = UUID.randomUUID().toString()
        mDataHolder.put(dataHolderId, data)
        return dataHolderId
    }

    fun popDataHolder(key: String?): Any {
        val obj: String = mDataHolder.get(key) as String
        //pop된 데이터는 홀더에서 제거
        mDataHolder.remove(key)
        return obj
    }
}
