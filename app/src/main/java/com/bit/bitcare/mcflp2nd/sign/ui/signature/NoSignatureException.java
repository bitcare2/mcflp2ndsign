package com.bit.bitcare.mcflp2nd.sign.ui.signature;

public class NoSignatureException extends RuntimeException {

    public NoSignatureException(String value){
        super(value);
    }

}
