package com.bit.bitcare.mcflp2nd.sign.ui

import android.content.Intent
import android.os.Bundle
import android.text.InputType
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.error.ANError
import com.androidnetworking.interfaces.JSONObjectRequestListener
import com.bit.bitcare.mcflp2nd.sign.R
import com.bit.bitcare.mcflp2nd.sign.base.BaseActivity
import com.bit.bitcare.mcflp2nd.sign.base.BaseApplication
import com.bit.bitcare.mcflp2nd.sign.model.*
import com.bit.bitcare.mcflp2nd.sign.utils.Define
import com.bit.bitcare.mcflp2nd.sign.utils.Dlog
import com.bit.bitcare.mcflp2nd.sign.utils.PreferenceUtil
import com.bit.bitcare.mcflp2nd.util.Util.Companion.classifyFormType
import com.google.firebase.iid.FirebaseInstanceId
import com.google.gson.internal.LinkedTreeMap
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_certification_information.*
import org.json.JSONObject
import java.util.regex.Pattern
import com.google.gson.reflect.TypeToken.getParameterized
import com.google.gson.stream.JsonReader
import kotlinx.android.synthetic.main.activity_certification_code.*
import java.io.StringReader
import java.net.URLEncoder


class CertificationInformationActivity : BaseActivity() {

    private lateinit var checkLoginKeyResultVo:CheckLoginKeyResultVo
    var certificationNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_certification_information)

        checkLoginKeyResultVo = gson.fromJson(PreferenceUtil(this).getStringExtra("checkLoginKeyResultVo"),  CheckLoginKeyResultVo::class.java)

        //인증 타입에 따른 안내 문구 및 edittext 세팅
        when(checkLoginKeyResultVo.loginType){
            "03" -> {
                tv_certification_information_comment.setText(R.string.certification_information_contents_phone_number)
                et_certification_information_input_key.hint = resources.getString(R.string.certification_information_only_number)
                et_certification_information_input_key.inputType = InputType.TYPE_CLASS_NUMBER
            }
            "04" -> {
                tv_certification_information_comment.setText(R.string.certification_information_contents_doctor_license_number)
                et_certification_information_input_key.hint = resources.getString(R.string.certification_information_only_number)
                et_certification_information_input_key.inputType = InputType.TYPE_CLASS_NUMBER
            }
            "05" -> {
                tv_certification_information_comment.setText(R.string.certification_information_contents_professional_doctor_license_phone_number)
                et_certification_information_input_key.hint = resources.getString(R.string.certification_information_enter)
                et_certification_information_input_key.inputType = InputType.TYPE_CLASS_TEXT
            }
            "08" -> {
                tv_certification_information_comment.setText(R.string.certification_information_contents_phone_number)
                et_certification_information_input_key.hint = resources.getString(R.string.certification_information_only_number)
                et_certification_information_input_key.inputType = InputType.TYPE_CLASS_NUMBER
            }
        }
        et_certification_information_input_key.imeOptions = EditorInfo.IME_ACTION_DONE
        et_certification_information_input_key.setOnEditorActionListener(TextView.OnEditorActionListener { textView, i, keyEvent ->
            when(i){
                EditorInfo.IME_ACTION_DONE ->{
                    doLogin()
                }
            }
            return@OnEditorActionListener false
        })
        bt_certification_information.setOnClickListener {
            doLogin()
        }
    }

    private fun doLogin(){
        if("03".equals(checkLoginKeyResultVo.loginType) || "08".equals(checkLoginKeyResultVo.loginType)){
            var comparisonTelNumber = et_certification_information_input_key.getText().toString().trim()
            var convertedTelNumber = makePhoneNumber(comparisonTelNumber)

            if(comparisonTelNumber.length != 0){
                if("".equals(convertedTelNumber)){
                    return
                }else{
                    certificationNumber = convertedTelNumber!! //변환된 번호 넣음
                }
            }else{
                Toast.makeText(this,"전화번호를 입력해 주세요.", Toast.LENGTH_LONG).show()
                return
            }
        }else{
            certificationNumber = et_certification_information_input_key.getText().toString().trim() //전화번호 아닌 의사면허 번호나 전문의 번호는 그냥 넣음
        }

        val fcmToken = FirebaseInstanceId.getInstance().token

        if (fcmToken != null) {
            Dlog.e("fcmToken = $fcmToken")
        }else{
            Dlog.e("fcmToken is null")
            Toast.makeText(this@CertificationInformationActivity,"토큰을 만드는데 실패했습니다.",Toast.LENGTH_LONG).show()
        }

        if(checkLoginKeyResultVo.userSN == null){
            checkLoginKeyResultVo.userSN = ""
        }

        certificationNumber = URLEncoder.encode(certificationNumber, "utf-8")

        Dlog.e("/sign/login")
        AndroidNetworking.post(Define.serverIP+"/sign/login")
                .addBodyParameter("loginType", checkLoginKeyResultVo.loginType) //  03: 웹사이트인증 C, 04: 서식서명인증 S, 05: 협진인증 J, 08 등록자서명인증 A
                .addBodyParameter("appDeviceSN", checkLoginKeyResultVo.appDeviceSN)
                .addBodyParameter("loginSN", checkLoginKeyResultVo.loginSN)
                .addBodyParameter("deviceSID", PreferenceUtil(this@CertificationInformationActivity).getStringExtra("androidID"))
                .addBodyParameter("deviceRegID", fcmToken)
                .addBodyParameter("userSN", checkLoginKeyResultVo.userSN)
                .addBodyParameter("number", certificationNumber)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        try {
                            Dlog.e("onCreate 받아옴??")
                            Dlog.e(response.toString())

                            var signLoginVo = gson.fromJson(response.toString(), SignLoginVo::class.java)
                            Dlog.e("signLoginVo.toString() : " + signLoginVo.toString())
                            Dlog.e("signLoginVo.result.toString() : " + signLoginVo.result.toString())
                            Dlog.e("gson.toJson(signLoginVo.token : " + gson.toJson(signLoginVo.token).replace("\"", ""))
                            Dlog.e("signLoginVo.result.userSN : " + signLoginVo.result.userSN)

                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("token", gson.toJson(signLoginVo.token).replace("\"", ""))
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("signLoginVo", gson.toJson(signLoginVo))
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("signLoginResultVo", gson.toJson(signLoginVo.result))
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("userSN", signLoginVo.result.userSN)
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("userName", signLoginVo.result.userName)
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("orgName", signLoginVo.result.orgName)


                            Dlog.e("signLoginVo.result.signFormSN : " + signLoginVo.result.signFormSN)
                            if (signLoginVo.result.signFormSN == null) {
                                val intent = Intent(this@CertificationInformationActivity, MainActivity::class.java)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
                                startActivity(intent)
                                overridePendingTransition(0, 0)
                                finish()
                            } else {
                                getFormInfo()
                            }
                        }catch (e:Exception){
                            Toast.makeText(this@CertificationInformationActivity,"올바른 번호를 입력해주세요.",Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("onCreate 에러??")
                        Toast.makeText(this@CertificationInformationActivity,"올바른 번호를 입력해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

    fun getFormInfo(){
        var token = PreferenceUtil(this).getStringExtra("token")
        Dlog.e("getFormInfo token : " + token)
        var signLoginResultVo = gson.fromJson(PreferenceUtil(this).getStringExtra("signLoginResultVo"),  SignLoginResultVo::class.java)

        AndroidNetworking.post(Define.serverIP+"/sign/getFormInfo")
                .addBodyParameter("token", token)
                .addBodyParameter("signFormSN", signLoginResultVo.signFormSN)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(object : JSONObjectRequestListener {
                    override fun onResponse(response: JSONObject) {
                        Dlog.e("getFormInfo 받아옴??")
                        Dlog.e(response.toString())

                        var getFormInfo: LinkedTreeMap<String, Any> = gson.fromJson(response.toString(), object : TypeToken<LinkedTreeMap<String, Any>>() {}.type)

                        Dlog.e(getFormInfo.get("serviceCode").toString())

                        if("100.0".equals(getFormInfo.get("serviceCode").toString()) || "100".equals(getFormInfo.get("serviceCode").toString())) {
                            Dlog.e("getFormInfo.get(\"token\") : " + getFormInfo.get("token") as String)
                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("token", getFormInfo.get("token") as String)

                            var getFormInfoTypeVo: GetFormInfoTypeVo = gson.fromJson(gson.toJson(getFormInfo.get("formType")), GetFormInfoTypeVo::class.java)
                            Dlog.e("getFormInfoTypeVo : " + getFormInfoTypeVo.toString())

                            PreferenceUtil(this@CertificationInformationActivity).putStringExtra("signFormSN", getFormInfoTypeVo.signFormSN)
                            Dlog.e("getFormInfoTypeVo.signFormSN : " + getFormInfoTypeVo.signFormSN)

                            if(getFormInfo.get("userInfo") != null){
                                var userInfo: UserInfoVO = gson.fromJson(gson.toJson(getFormInfo.get("userInfo")), UserInfoVO::class.java)
                                Dlog.e("userInfo : " + userInfo.toString())

                                var bundle = classifyFormType(getFormInfo, getFormInfoTypeVo, userInfo, gson)

                                if (bundle.getString("ozFileName").equals("잘못된 서식요청 코드") || bundle.getString("ozFileName").equals("잘못된 서식요청") || bundle.getString("ozFileName").equals("결과값 없음") || bundle.getString("ozFileName").equals("데이터 파싱 에러")) {
                                    Toast.makeText(this@CertificationInformationActivity, bundle.getString("ozFileName"), Toast.LENGTH_LONG).show()
                                } else {
                                    bundle.putString("formInfo", gson.toJson(getFormInfoTypeVo))
                                    goToForm(bundle)
                                }
                            }else{
                                Toast.makeText(this@CertificationInformationActivity, "사용자 정보를 확인할 수 없습니다.",Toast.LENGTH_LONG).show()
                            }
                        }else{
                            Toast.makeText(this@CertificationInformationActivity, getFormInfo.get("serviceMsg") as String,Toast.LENGTH_LONG).show()
                        }
                    }

                    override fun onError(error: ANError) {
                        // handle error
                        Dlog.e("getFormInfo 에러??")
                        Toast.makeText(this@CertificationInformationActivity,"올바른 번호를 입력해주세요.",Toast.LENGTH_LONG).show()
                    }
                })
    }

    fun goToForm(bundle: Bundle){
        if("".equals(PreferenceUtil(this@CertificationInformationActivity).getStringExtra("userSN"))){
            val intent = Intent(this, EFormViewerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            intent.putExtras(bundle)
            startActivity(intent)
            overridePendingTransition(0,0)
            finish()
        }else{  //userSN 이 있으면 자동로그인 -> 메인 깔고 폼으로 이동
            val intent = Intent(this, MainActivity::class.java)
            val intent2 = Intent(this, EFormViewerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
            startActivity(intent)
            overridePendingTransition(0,0)
            intent2.putExtras(bundle)
            startActivity(intent2)
            finish()
        }

    }

    private fun makePhoneNumber(phoneNumber:String): String? {
        val regEx = "(\\d{3})(\\d{3,4})(\\d{4})"
        if (!Pattern.matches(regEx, phoneNumber)) {
            Toast.makeText(this,"올바른 휴대폰 번호를 입력해 주세요",Toast.LENGTH_LONG).show()
            return ""
        }
        return phoneNumber.replace((regEx).toRegex(), "$1-$2-$3")
    }


}