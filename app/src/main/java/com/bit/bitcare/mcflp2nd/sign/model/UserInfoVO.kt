package com.bit.bitcare.mcflp2nd.sign.model


data class UserInfoVO(
// @ApiModelProperty(value = "사용자일련번호", example = "123456")
        var usersn: Int?,            // bigint NOT NULL DEFAULT nextval('seq_t_user'::regclass), -- 사용자일련번호
// @ApiModelProperty(value = "사용자로그인아이디", example = "123456")
        var userid: String?,            // character varying(50), -- 사용자로그인아이디
// @ApiModelProperty(value = "등록기관번호", example = "R17100002")
        var orgregno: String?,        // character varying(9), -- 등록기관번호
// @ApiModelProperty(value = "의료기관번호", example = "M17100003")
        var orgmedno: String?,        // character varying(9), -- 의료기관번호
// @ApiModelProperty(value = "사용자 구분코", example = "01")
        var userclscode: String?,    // character varying(2) NOT NULL, -- 사용자 구분코드
// @ApiModelProperty(value = "사용자 상태코드", example = "01")
        var userstatuscode: String?,    // character varying(2) NOT NULL, -- 사용자 상태코드
// @ApiModelProperty(value = "권한번호", example = "1")
        var roleno: String?,            // integer, -- 권한번호
// @ApiModelProperty(value = "관리자여부", example = "Y")
        var adminyn: String?,        // character varying(1), -- 관리자여부
// @ApiModelProperty(value = "기관담당자여부", example = "Y")
        var orgchargeyn: String?,    // character varying(1), -- 기관담당자여부
// @ApiModelProperty(value = "기관부관리자여부", example = "Y")
        var orgsubadminyn: String?,    // character varying(1), -- 기관부관리자여부
// @ApiModelProperty(value = "면허번호", example = "123456")
        var licence: String?,        // character varying(10), -- 면허번호
// @ApiModelProperty(value = "사용자이름", example = "123456")
        var usernm: String?,            // character varying(20) NOT NULL, -- 사용자이름
// @ApiModelProperty(value = "사용자 암호", example = "123456")
        var userpwd: String?,        // character varying(500), -- 사용자 암호
// @ApiModelProperty(value = "사용자 암호(암호화)", example = "123456")
        var userpwdenc: String?,
// @ApiModelProperty(value = "개인공인인증서DN", example = "123456")
        var prvcertdn: String?,        // character varying(256), -- 개인공인인증서DN
// @ApiModelProperty(value = "SRP6A 인증키값 SALT", example = "123456")
        var salt: String?,            // character varying(100), -- SRP6A 인증키값 SALT
// @ApiModelProperty(value = "SRP6A 인증키값 VARIFIER", example = "123456")
        var varifier: String?,        // character varying(100), -- SRP6A 인증키값 VARIFIER
// @ApiModelProperty(value = "휴대폰번호", example = "123456")
        var hptn: String?,            // character varying(14), -- 휴대폰번호
// @ApiModelProperty(value = "이메일", example = "123456")
        var email: String?,            // character varying(100), -- 이메일
// @ApiModelProperty(value = "직책", example = "123456")
        var position: String?,        // character varying(20), -- 직책
// @ApiModelProperty(value = "최근 로그인일시", example = "123456")
        var logindate: String?,        // timestamp without time zone, -- 최근 로그인일시
// @ApiModelProperty(value = "최근 로그인 IP", example = "123456")
        var loginip: String?,        // character varying(15), -- 최근 로그인 IP
// @ApiModelProperty(value = "총 로그인 횟수", example = "123456")
        var logincnt: String?,        // integer, -- 총 로그인 횟수
// @ApiModelProperty(value = "마지막 암호 변경일자", example = "123456")
        var lastpwdchgdate: String?,    // date, -- 마지막 암호 변경일자
// @ApiModelProperty(value = "서명이미지", example = "123456")
        var signimgfilegrpno: String?, // bigint, -- 서명이미지
// @ApiModelProperty(value = "등록프로그램", example = "123456")
        var enterpgm: String?,        // character varying(200) NOT NULL, -- 등록프로그램
// @ApiModelProperty(value = "등록일시", example = "123456")
        var enterdate: String?,        // timestamp without time zone NOT NULL, -- 등록일시
// @ApiModelProperty(value = "등록자아이디", example = "123456")
        var enterid: String?,        // character varying(50) NOT NULL, -- 등록자아이디
// @ApiModelProperty(value = "등록자명", example = "123456")
        var enternm: String?,        // character varying(50) NOT NULL, -- 등록자명
// @ApiModelProperty(value = "수정프로그램", example = "123456")
        var updatepgm: String?,        // character varying(200), -- 수정프로그램
// @ApiModelProperty(value = "수정일시", example = "123456")
        var updatedate: String?,        // timestamp without time zone, -- 수정일시
// @ApiModelProperty(value = "수정자아이디", example = "123456")
        var updateid: String?,        // character varying(50), -- 수정자아이디
// @ApiModelProperty(value = "수정자명", example = "123456")
        var updatenm: String?,        // character varying(50), -- 수정자명
// @ApiModelProperty(value = "등록자여부", example = "Y")
        var reguseryn: String?,
// @ApiModelProperty(value = "상담자여부", example = "Y")
        var adviuseryn: String?,
// @ApiModelProperty(value = "의사여부", example = "Y")
        var docuseryn: String?,
// @ApiModelProperty(value = "윤리위원회 담당자 여부", example = "Y")
        var ecchargeyn: String?,
// @ApiModelProperty(value = "부가서식저장여부", example = "Y")
        var addformsavyn: String?,
// @ApiModelProperty(value = "등록기관명", example = "등록기관명")
        var orgregnm: String?,
// @ApiModelProperty(value = "등록기관소재지", example = "등록기관소재지")
        var orgregaddr: String?,
// @ApiModelProperty(value = "의로기관명", example = "의로기관명")
        var orgmednm: String?,
// @ApiModelProperty(value = "등록기관번호", example = "1")
        var regorgsn: String?,
// @ApiModelProperty(value = "의료기관번호", example = "1")
        var medorgsn: String?,
// @ApiModelProperty(value = "등록기관대표전화번호", example = "02-111-2222")
        var regorgtn: String?,
// @ApiModelProperty(value = "의료기관대표전화번호", example = "02-111-2222")
        var medorgtn: String?,
// @ApiModelProperty(value = "의료기관주소1", example = "1")
        var medaddr1: String?,
// @ApiModelProperty(value = "의료기관주소2", example = "1")
        var medaddr2: String?,
// @ApiModelProperty(value = "의료기관 우편번호", example = "1")
        var medzipno: String?,
// @ApiModelProperty(value = "의료기관 법정동 코드", example = "1")
        var medbjdcode: String?,
// @ApiModelProperty(value = "등록기관 요양기관번호", example = "1")
        var regmcorgno: String?,
// @ApiModelProperty(value = "의료기관 요양기관번호", example = "1")
        var medmcorgno: String?
)
