package com.bit.bitcare.mcflp2nd.sign.model

data class CheckLoginKeyResultVo(
    var loginSN: String,
    var loginType: String,
    var userSN: String,
    var phoneNumber: String,
    var licenceNumber: String,
    var specialistNumber: String,
    var signFormSN: String,
    var deviceSID: String,
    var appDeviceSN: String
)