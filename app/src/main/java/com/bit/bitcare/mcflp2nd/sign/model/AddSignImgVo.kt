package com.bit.bitcare.mcflp2nd.sign.model

import java.util.*

data class AddSignImgVo(
        var serviceCode: String?,
        var serviceMsg: String?,
        var token: String?,
        var result: AddSignImgResultVo
)