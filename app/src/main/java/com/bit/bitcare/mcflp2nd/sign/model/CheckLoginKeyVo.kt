package com.bit.bitcare.mcflp2nd.sign.model

data class CheckLoginKeyVo(
    var serviceCode: String,
    var serviceMsg: String,
    var result: CheckLoginKeyResultVo
)