package com.bit.bitcare.mcflp2nd.sign.model

data class AddSignImgResultVo(
        var signRequestSN: String?,
        var requestCode: String?,
        var formSN: String?,
        var fileSN: String?,
        var fileName: String?,
        var fileURL: String?,
        var fileSize: String?,
        var fileType: String?,
        var signText: String?,
        var signDate: String?,
        var stateCode: String?,
        var updateID: String?,
        var updateNM: String?
)

