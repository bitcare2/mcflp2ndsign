package com.bit.bitcare.mcflp2nd.sign.model

import java.util.*

data class GetFormListItemVo(
        var signFormSN: String,
        var formName: String,
        var requestName: String,
        var requestStateName: String,
        var formNumber: String,
        var formSN: String,
        var userName: String,
        var userGender: String,
        var requestDate: String
)