package com.bit.bitcare.mcflp2nd.sign.model


data class EdffamconfrDetailInfo(
    var edffamconfrDetailInfo: EdffamconfrDetailInfoVO?,
    var signList: BasicEdffamconfrSignInfoVO
)