package com.bit.bitcare.mcflp2nd.sign.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;


import com.bit.bitcare.mcflp2nd.sign.R;
import com.bit.bitcare.mcflp2nd.sign.ui.MainActivity;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


/**
 * Created by acid on 2016-11-21.
 * 데이터 수신용 클래스
 */

public class FcmListenerService extends FirebaseMessagingService {

    private static final String TAG = "FCM";
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onMessageReceived(RemoteMessage message) {
        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Log.e(TAG, "From: " + message.getFrom());

//        Log.e(TAG, "From: " + message.getNotification().getBody());

        // Check if message contains a data payload.
        if (message.getData().size() > 0) { //도즈모드 풀기 위함
            Log.e(TAG, "Message data payload: " + message.getData());

            if(!message.getData().get("title").equals("title")) {  //Doze모드 푸는건 노티 띄우지 않음
                sendNotification(message.getData().get("title"), message.getData().get("body"));
            }
        }

        Log.e("FCM getFrom : ", message.getFrom());
        Log.e("FCM getMessageId : ", message.getMessageId());
    }

    private NotificationManager notifManager;

    private void sendNotification(String title, String messageBody) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationCompat.Builder builder;

            if (notifManager == null) {
                notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
            }

            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel mChannel = notifManager.getNotificationChannel("ch2");
            if (mChannel == null) {
                mChannel = new NotificationChannel("ch2", "name", importance);
                mChannel.setDescription("zz");
                mChannel.enableVibration(true);
                mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notifManager.createNotificationChannel(mChannel);
            }
            builder = new NotificationCompat.Builder(this, "ch2");

//            intent = new Intent(this, MainActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
//            pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

            builder.setContentTitle(title)
                    .setContentText(messageBody)// required
                    .setSmallIcon(R.drawable.ic_notification) // required
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent)
                    .setTicker("BITCare")
                    .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});

            Notification notification = builder.build();
            notifManager.notify(2002, notification);
        }else{
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,"ch1");
            builder.setSmallIcon(R.drawable.ic_notification)
                    .setColor(ResourcesCompat.getColor(getBaseContext().getResources(), R.color.colorPrimary, null))
                    .setLargeIcon(BitmapFactory.decodeResource(getBaseContext().getResources(), R.mipmap.ic_launcher))
                    .setTicker("BITCare")
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(title)
                    .setContentText(messageBody)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingIntent)
                    .setAutoCancel(true)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody));

            NotificationManagerCompat.from(this).notify(2001, builder.build());
        }
    }


}